//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/models"
	constants "git.forms.io/isaving/sv/ssv0000001/constant"
	"git.forms.io/isaving/sv/ssv0000001/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv0000001Controller struct {
    controllers.CommTCCController
}

func (*Ssv0000001Controller) ControllerName() string {
	return "Ssv0000001Controller"
}

// @Desc Ac020001Controller Controller
// @Description Entry
// @Param ac020001 body models.SSV0000001I true "body for model content"
// @Success 200 {object} models.SSV0000001O
// @router /ssv0000001 [post]
// @Date 2020-11-25
func (c *Ssv0000001Controller) Ssv0000001() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv0000001Controller.Ssv0000001 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv0000001I := &models.SSV0000001I{}
	if err :=  models.UnPackRequest(c.Req.Body, ssv0000001I); err != nil {
		c.SetServiceError(errors.New(err, constants.ERRCODE2))
		return
	}
  if err := ssv0000001I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv0000001 := &services.Ssv0000001Impl{} 
    ssv0000001.New(c.CommTCCController)
    ssv0000001.SSV0000001I = ssv0000001I
	//ssv0000001.DlsInterface = &commclient.DlsOperate{}
	ssv0000001Compensable := services.Ssv0000001Compensable

	//ssv0000001.SSV0000001I.
	proxy, err := aspect.NewDTSProxy(ssv0000001, ssv0000001Compensable, c.DTSCtx)
	if err != nil {
		log.Errorf("Register DTS Proxy failed. %v", err)
		c.SetServiceError(errors.Errorf(constant.PROXYREGFAILD, "Register DTS Proxy failed. %v", err))
		return
	}

	rets := proxy.Do(ssv0000001I)

	if len(rets) < 1 {
		log.Error("DTS proxy executed service failed, not have any return")
		c.SetServiceError(errors.Errorf(constant.PROXYFAILD,"DTS proxy executed service failed, %v", "not have any return"))
		return
	}

	if e := rets[len(rets)-1].Interface(); e != nil {
		log.Errorf("DTS proxy executed service failed %v", err)
		c.SetServiceError(e)
		return
	}

	rsp := rets[0].Interface()
	if ssv0000001O, ok := rsp.(*models.SSV0000001O); ok {
		if responseBody, err := models.PackResponse(ssv0000001O); err != nil {
			c.SetServiceError(err)
		} else {
			c.SetAppBody(responseBody)
		}
	}
} 
// @Title Ssv0000001 Controller
// @Description ssv0000001 controller
// @Param Ssv0000001 body models.SSV0000001I true body for SSV0000001 content
// @Success 200 {object} models.SSV0000001O
// @router /create [post]
func (c *Ssv0000001Controller) SWSsv0000001() {
	//Here is to generate API documentation, no need to implement methods
}
