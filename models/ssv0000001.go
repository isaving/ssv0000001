//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV0000001I struct {
	PrductId		string `validate:"required",json:"PrductId"`		//产品号
	PrductNm        int    `validate:"required",json:"PrductNm"`        //产品顺序号
	Cur             string `validate:"required",json:"Cur"`				//币种
	CashTranFlag    string `validate:"required",json:"CashTranFlag"`    //钞汇标识
	CstmrId         string `validate:"required",json:"CstmrId"`         //客户编号
	CstmrTyp        string `validate:"required",json:"CstmrTyp"`        //客户类型
	AccuntNme       string `validate:"required",json:"AccuntNme"`       //账户名称
	UsgCod          string `validate:"required",json:"UsgCod"`          //用途代码
	WdrwlMthd       string `validate:"required",json:"WdrwlMthd"`       //支取方式
	AccPsw          string `validate:"required",json:"AccPsw"`          //账户密码
	CshDpWdFlg      string `validate:"required",json:"CshDpWdFlg"`      //现金存取标识
	OpnAmt          string `validate:"required",json:"OpnAmt"`          //开户金额
	AccCnclFlg      string `validate:"required",json:"AccCnclFlg"`      //销户启用标识
	AutCnl          string `validate:"required",json:"AutCnl"`          //是否允许自动销户
	DytoCncl        string `validate:"required",json:"DytoCncl"`        //自动销户宽限天数
	SttmntFlg       string `validate:"required",json:"SttmntFlg"`       //对账单标识
	WthdrwlMthd     string `validate:"required",json:"WthdrwlMthd"`     //取款方式
	OvrDrwFlg       string `validate:"required",json:"OvrDrwFlg"`       //透支标识
	CstmrCntctAdd   string `validate:"required",json:"CstmrCntctAdd"`   //客户联系地址
	CstmrCntctPh    string `validate:"required",json:"CstmrCntctPh"`    //客户联系电话
	CstmrCntctEm    string `validate:"required",json:"CstmrCntctEm"`    //邮箱地址
	BnCrtAcc        string `validate:"required",json:"BnCrtAcc"`        //开户行所
	BnAcc           string `validate:"required",json:"BnAcc"`           //账务行所
	AccOpnDt        string `validate:"required",json:"AccOpnDt"`        //开户日期
	AccOpnEm        string `validate:"required",json:"AccOpnEm"`        //开户柜员
	ChkDTrnFlag     string `validate:"required",json:"ChkDTrnFlag"`     //是否需要堵重检查标志
	ChkDTrnCode     string `json:"ChkDTrnCode"`                         //堵重随机码
	RequrstId       string `json:"RequrstId"`                           //外围流水号
}

type SSV0000001O struct {
	AgreementID		string		//合约号
	AgreementType	string		//合约类型

}

// @Desc Build request message
func (o *SSV0000001I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV0000001I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV0000001O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV0000001O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV0000001I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV0000001I) GetServiceKey() string {
	return "ssv0000001"
}
