//Version: v0.0.1
package services

import (
	"encoding/base64"
	"git.forms.io/isaving/models"
	constants "git.forms.io/isaving/sv/ssv0000001/constant"
	"git.forms.io/isaving/sv/ssv0000001/util"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	commClient "git.forms.io/universe/comm-agent/client"
	commRsa "git.forms.io/universe/common/crypto/rsa"
	"git.forms.io/universe/common/json"
	dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/solapp-sdk/config"
	"git.forms.io/universe/solapp-sdk/log"
	"strconv"
	"time"
)
var Ssv0000001Compensable = dtsClient.Compensable{
	TryMethod:     "TrySsv0000001",
	ConfirmMethod: "ConfirmSsv0000001",
	CancelMethod:  "CancelSsv0000001",
}

type Ssv0000001 interface {
	TrySsv0000001(*models.SSV0000001I) (*models.SSV0000001O, error)
	ConfirmSsv0000001(*models.SSV0000001I) (*models.SSV0000001O, error)
	CancelSsv0000001(*models.SSV0000001I) (*models.SSV0000001O, error)
}

type Ssv0000001Impl struct {
	services.CommonTCCService
	SSV0000001O         *models.SSV0000001O
	SSV0000001I         *models.SSV0000001I
	AgreementId			string
	Account				string
	DcnId				string
	PriKey				string
	GenerateKey			string
	saltPsw				string

	MediumNm			string
	MediumType				string

	SSV1000008O			*models.SSV1000008O
	SSV1000009O			*models.SSV1000009O
	SSV9000001O			*models.SSV9000001O
	SSV9000002O			*models.SSV9000002O

	SSV1000021O			*models.SSV1000021O
}
// @Desc Ssv0000001 process
// @Author
// @Date 2020-12-04
func (impl *Ssv0000001Impl) TrySsv0000001(ssv0000001I *models.SSV0000001I) (ssv0000001O *models.SSV0000001O, err error) {

	impl.SSV0000001I = ssv0000001I

	//生成合约号
	impl.AgreementId = impl.HandAgreementId()
	log.Infof("AgreementId:%v", impl.AgreementId)

	if "" == impl.SSV0000001I.MediumNm || "" == impl.SSV0000001I.MediumType {
		impl.MediumNm = impl.AgreementId
		impl.MediumType = "VCD"
	} else {
		impl.MediumNm = ssv0000001I.MediumNm
		impl.MediumType = ssv0000001I.MediumType
	}

	//产品信息查询
	if err := impl.SV900002(); nil != err {
		return nil, err
	}
	if "1" != impl.SSV9000002O.PrductStatus {
		return nil, errors.New("", constants.ERRCODE8)
	}
	//客户与合约的关系--查询BFS
	if err := impl.SV100008(); nil != err {
		return nil, err
	}
	//产品是否有限买次数
	if impl.SSV1000008O.ContractCount > impl.SSV9000002O.PrductBuyTime {
		log.Errorf("超出购买限制次数，ContractCount：%v > %v", impl.SSV1000008O.ContractCount, impl.SSV9000002O.PrductBuyTime)
		return nil, errors.New(err, constants.ERRCODE13)
	}

	//注册DLS--以客户号，合约号 为分片要素
	if err := impl.CallDlsCreateAGM(); nil != err {
		return nil, err
	}
	//注册DLS--以客户号，合约号 为分片要素
	if err := impl.CallDlsCreateCus(); nil != err {
		return nil, err
	}
	//开立核算账户
	if err := impl.SV100009(); nil != err {
		return nil, err
	}
	//get sv pri_key
	if err = impl.SV100021(); nil != err {
		return nil, err
	}
	//get sv pri_key
	impl.PriKey = HandResult(impl.SSV1000021O, impl.SSV0000001I.KeyVersion)
	//解密 加盐 加码
	if err = impl.GeneratePsw(); nil != err {
		return nil, err
	}
	//新增合约开户
	if err := impl.SV900001(); nil != err {
		return nil, err
	}
	//客户合约关系新增
	if err := impl.SV100010(); nil != err {
		return nil, err
	}

	//新增合约介质关系服务
	if err := impl.SV900006(); nil != err {
		return nil, err
	}
	//合约利率关系新增
	if err := impl.SV100015(); nil != err {
		return nil, err
	}
	ssv0000001O = &models.SSV0000001O{
		AgreementID: impl.AgreementId,
		AgreementType: "10" + impl.SSV9000002O.DepositNature,
	}

	return ssv0000001O, nil
}

func (impl *Ssv0000001Impl) ConfirmSsv0000001(ssv0000001I *models.SSV0000001I) (ssv0000001O *models.SSV0000001O, err error) {
	log.Debug("Start confirm ssv0000001")
	return nil, nil
}

func (impl *Ssv0000001Impl) CancelSsv0000001(ssv0000001I *models.SSV0000001I) (ssv0000001O *models.SSV0000001O, err error) {
	log.Debug("Start cancel ssv0000001")
	return nil, nil
}


func (impl *Ssv0000001Impl) HandAgreementId() string {
	//卡BIN(8位)+DCN后四位（4位）+序号（10位，DCN内递增）+校验位（1位）
	//如：62222222 0001 0000000001 6
	Bin := constants.CatBin
	Dcn := config.CmpSvrConfig.DstDcn
	if 4 > len(Dcn) {
		Dcn = "0000"
	}else {
		Dcn = Dcn[len(Dcn)-4:]
	}
	UnixNano := strconv.FormatInt(time.Now().UnixNano(), 10)
	UnixNano = UnixNano[:11]

	return Bin + Dcn + UnixNano
}

//产品信息查询
func (impl *Ssv0000001Impl) SV900002() error {
	SSV9000002I := &models.SSV9000002I{
		PrductId:      impl.SSV0000001I.PrductId,
		PrductNm:      impl.SSV0000001I.PrductNm,
		CUR:           impl.SSV0000001I.Cur,
		AgreementType:	"10001",	//合约类型
		PeriodFlag:		"1",
	}
	rspBody, err := SSV9000002I.PackRequest()
	if nil != err {
		return errors.New(err, constants.ERRCODE1)
	}
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_SCM, constant.DLS_ID_COMMON, constants.SV900002, rspBody);
	if err != nil{
		log.Errorf("SV900002, err:%v",err)
		return err
	}
	SSV9000002O := &models.SSV9000002O{}
	if err := SSV9000002O.UnPackResponse(resBody); nil != err {
		log.Errorf("UnPackResponse SSV9000002O Fail")
		return errors.New(err, constants.ERRCODE2)
	}
	impl.SSV9000002O = SSV9000002O
	return nil
}

//客户与合约的关系
func (impl *Ssv0000001Impl) SV100008() error {
	Request := &models.SSV1000008I{
		CustId:			impl.SSV0000001I.CstmrId,	//客户号
		ContractType: 	"10" + impl.SSV9000002O.DepositNature,	//合约类型
	}
	rspBody, err := Request.PackRequest()
	if nil != err {
		return errors.New(err, constants.ERRCODE1)
	}
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constants.SV100008, rspBody);
	if err != nil{
		log.Errorf("SV100008, err:%v",err)
		return err
	}
	Response := &models.SSV1000008O{}
	if err := Response.UnPackResponse(resBody); nil != err {
		log.Errorf("UnPackResponse SSV1000008O Fail")
		return errors.New(err, constants.ERRCODE2)
	}
	impl.SSV1000008O = Response

	return nil
}

//开立核算账户
func (impl *Ssv0000001Impl) SV100009() error {
	Request := &models.SSV1000009I{
		CustId:	impl.SSV0000001I.CstmrId,	//客户号
		Account: 	"10" + impl.SSV9000002O.DepositNature,	//合约类型
		Amount:	impl.SSV0000001I.OpnAmt,				//开户金额
		AlloWithdTimes: 100,							//允许支取次数
		Currency:		impl.SSV0000001I.Cur,			//币别
		ObjPublic:		"test",							//交易公共信息
		BranchNo:		impl.SSV0000001I.BnCrtAcc,		//机构编号
		TellerId:		"1",							//员工编号
	}
	rspBody, err := Request.PackRequest()
	if nil != err {
		return errors.New(err, constants.ERRCODE1)
	}
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constants.SV100009, rspBody);
	if err != nil{
		log.Errorf("SV100009, err:%v",err)
		return err
	}
	Response := &models.SSV1000009O{}
	if err := Response.UnPackResponse(resBody); nil != err {
		log.Errorf("UnPackResponse SSV1000009O Fail")
		return errors.New(err, constants.ERRCODE2)
	}
	impl.SSV1000009O = Response
	return nil
}

func (impl *Ssv0000001Impl) SV100010() error {
	Request := &models.SSV1000010I{
		CustId:       impl.SSV0000001I.CstmrId,	//客户号
		Account:      impl.AgreementId,
		ContactType:  "10" + impl.SSV9000002O.DepositNature,
		//AListContact: nil,
		//AListAddr:    nil,
		//ObjPublic:    models.SSV1000010IObjPublic{},
	}
	SSV1000010IAListContact := make([]models.SSV1000010IAListContact,0)
	SSV1000010IAListAddr	:= make([]models.SSV1000010IAListAddr,0)
	AListContact1 := models.SSV1000010IAListContact{
		ContactType:"09",
		Contact:impl.SSV0000001I.CstmrCntctPh,
		UseType:"0",
	}
	AListContact2 := models.SSV1000010IAListContact{
		ContactType:"04",
		Contact:impl.SSV0000001I.CstmrCntctEm,
		UseType:"0",
	}
	SSV1000010IAListContact = append(SSV1000010IAListContact, AListContact1, AListContact2)

	AListAddr := models.SSV1000010IAListAddr{
		Addr: impl.SSV0000001I.CstmrCntctAdd,
		UseType:"0",
		AddrType:"1",
	}
	SSV1000010IAListAddr = append(SSV1000010IAListAddr, AListAddr)

	Request.AListContact = SSV1000010IAListContact
	Request.AListAddr = SSV1000010IAListAddr
	Request.ObjPublic.BranchNo = impl.SSV0000001I.BnCrtAcc
	Request.ObjPublic.TellerId = impl.SSV0000001I.AccOpnEm

	rspBody, err := Request.PackRequest()
	if nil != err {
		return errors.New(err, constants.ERRCODE1)
	}
	_, err = impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constants.SV100010, rspBody);
	if err != nil{
		log.Errorf("SV100010, err:%v",err)
		return err
	}
	return nil
}

func (impl *Ssv0000001Impl) SV900001() error {
	SSV9000001I := &models.SSV9000001I{
		AgreementId      	: impl.AgreementId     	,
		AgreementType       : "10" + impl.SSV9000002O.DepositNature ,
		Currency     		: impl.SSV0000001I.Cur     		,
		CashtranFlag        : impl.SSV0000001I.CashTranFlag      ,
		AgreementStatus     : "0"   ,
		FreezeType          : "0"   ,
		TmpryPrhibition     : "0"   ,
		TermFlag            : impl.SSV9000002O.TermFlag          ,
		DepcreFlag          : impl.SSV9000002O.DepcreFlag        ,
		AccFlag             : impl.SSV0000001I.AccFlag           ,
		AccAcount           : impl.SSV1000009O.AccountingId      ,
		BegnTxDt            : time.Now().Format("2006-01-02"),
		AppntDtFlg          : impl.SSV9000002O.AppntDtFlg        ,
		AppntDt             : impl.SSV0000001I.AppntDt           ,
		PrductId            : impl.SSV0000001I.PrductId          ,
		PrductNm            : impl.SSV0000001I.PrductNm          ,
		CstmrId             : impl.SSV0000001I.CstmrId           ,
		CstmrTyp            : impl.SSV0000001I.CstmrTyp          ,
		AccuntNme           : impl.SSV0000001I.AccuntNme         ,
		UsgCod              : impl.SSV0000001I.UsgCod            ,
		WdrwlMthd           : impl.SSV0000001I.WdrwlMthd         ,
		AccPsw              : impl.saltPsw		           ,
		PswWrongTime        : 0      ,
		TrnWdrwmThd         : impl.SSV9000002O.TrnWdrwmThd       ,
		OpnAmt              : impl.SSV0000001I.OpnAmt            ,
		AccCnclFlg          : impl.SSV0000001I.AccCnclFlg        ,
		AutCnl              : impl.SSV0000001I.AutCnl            ,
		DytoCncl            : impl.SSV0000001I.DytoCncl          ,
		SttmntFlg           : impl.SSV0000001I.SttmntFlg         ,
		WthdrwlMthd         : impl.SSV0000001I.WthdrwlMthd       ,
		OvrDrwFlg           : impl.SSV0000001I.OvrDrwFlg         ,
		CstmrCntctAdd       : impl.SSV0000001I.CstmrCntctAdd     ,
		CstmrCntctPh        : impl.SSV0000001I.CstmrCntctPh      ,
		CstmrCntctEm        : impl.SSV0000001I.CstmrCntctEm      ,
		BnCrtAcc            : impl.SSV0000001I.BnCrtAcc          ,
		BnAcc               : impl.SSV0000001I.BnAcc             ,
		AccOpnDt            : impl.SSV0000001I.AccOpnDt          ,
		AccOpnEm            : impl.SSV0000001I.AccOpnEm          ,
		//AccCanclDt          : impl.SSV0000001I.AccCanclDt        ,
		//AccCanclEm          : impl.SSV0000001I.AccCanclEm        ,
		//AccCanclResn        : impl.SSV0000001I.AccCanclResn      ,
		LastUpDatetime      : time.Now().Format("2006-01-02") ,
		//LastUpBn            : impl.SSV0000001I.LastUpBn          ,
		//LastUpEm            : impl.SSV0000001I.LastUpEm          ,
		GlobalBizSeqNo		:	impl.SSV0000001I.GlobalBizSeqNo,
		SrcBizSeqNo			:	impl.SSV0000001I.SrcBizSeqNo,
		DepositNature		:	impl.SSV0000001I.DepositNature,
		MediumType			:	impl.SSV0000001I.MediumType,
		MediumNm			:	impl.SSV0000001I.MediumNm,
	}
	rspBody, err := SSV9000001I.PackRequest()
	if nil != err {
		return errors.New(err, constants.ERRCODE1)
	}
	resBody, err := impl.RequestSyncServiceWithDCN(impl.DcnId, constants.SV900001, rspBody);
	if err != nil{
		log.Errorf("SV900001, err:%v",err)
		return err
	}
	SSV9000001O := &models.SSV9000001O{}
	if err := impl.SSV9000001O.UnPackResponse(resBody); nil != err {
		log.Errorf("UnPackResponse SSV9000001O Fail")
		return errors.New(err, constants.ERRCODE2)
	}
	impl.SSV9000001O = SSV9000001O
	return nil
}

func (impl *Ssv0000001Impl) CallDlsCreateAGM() error {
	primaryDcn := commClient.DlsPrimaryDcn{}
	elements := make([]commClient.DlsElement, 0)
	element1 := commClient.DlsElement{
		ElementType: constant.DLS_TYPE_AGM,
		ElementClass:constant.DLS_TYPE_AGM,
		ElementId:   impl.AgreementId,
	}
	element2 := commClient.DlsElement{
		ElementType: constant.DLS_TYPE_MED,
		ElementClass:constant.DLS_TYPE_MED,
		ElementId:   impl.MediumNm + impl.MediumType,
	}
	elements = append(elements, element1, element2)
	body, _ := json.Marshal(commClient.DlsCreatePrimaryStruct{
		Option: 0,
		Dimesion: &commClient.DlsDimension{
			Topic: commClient.DlsTopic{
				TopicType: constant.TRN_TOPIC_TYPE,
				TopicId:   constants.SV900001,
			},
			DcnType: "",
		},
		Elements: elements,
	})
	dlsCreateRsp, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constants.DlsupCreateDTS, body)
	log.Debugf("DlsupCreateDTS dlsCreateRsp[%s]\n", dlsCreateRsp)
	if err != nil {
		return  errors.Errorf(constants.ERRCODE4, "Call DlsupCreateDTS failed error: [%s]", err)
	} else {
		retCode := json.Get(dlsCreateRsp, "Code").ToString()
		if retCode == "0" {
			primaryDcn.DcnType = json.Get(dlsCreateRsp, "Data", "DcnType").ToString()
			primaryDcn.DcnId = json.Get(dlsCreateRsp, "Data", "DcnId").ToString()
		} else{
			return errors.Errorf(constants.ERRCODE4, "%s", retCode+json.Get(dlsCreateRsp, "Message").ToString())
		}
	}
	impl.DcnId = primaryDcn.DcnId
	return nil
}

func (impl *Ssv0000001Impl) CallDlsCreateCus() error {
	primaryDcn := commClient.DlsPrimaryDcn{}
	elements := make([]commClient.DlsElement, 0)
	element1 := commClient.DlsElement{
		ElementType:  constant.DLS_TYPE_CUS,
		ElementClass: constant.DLS_TYPE_CUS,
		ElementId:    impl.SSV0000001I.CstmrId,
	}
	elements = append(elements, element1)
	body, _ := json.Marshal(commClient.DlsCreatePrimaryStruct{
		Option: 0,
		Dimesion: &commClient.DlsDimension{
			Topic: commClient.DlsTopic{
				TopicType: constant.TRN_TOPIC_TYPE,
				TopicId:   constants.SV900001,
			},
			DcnType: "",
		},
		Elements: elements,
	})
	dlsCreateRsp, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constants.DlsupCreateDTS, body)
	log.Debugf("DlsupCreateDTS dlsCreateRsp[%s]\n", dlsCreateRsp)
	if err != nil {
		return  errors.Errorf(constants.ERRCODE4, "Call DlsupCreateDTS failed error: [%s]", err)
	} else {
		retCode := json.Get(dlsCreateRsp, "Code").ToString()
		if "0" == retCode {
			primaryDcn.DcnType = json.Get(dlsCreateRsp, "Data", "DcnType").ToString()
			primaryDcn.DcnId = json.Get(dlsCreateRsp, "Data", "DcnId").ToString()
		} else if "664518" == retCode{
			return nil
		} else{
			return errors.Errorf(constants.ERRCODE4, "%s", retCode+json.Get(dlsCreateRsp, "Message").ToString())
		}
	}

	return nil
}


func (impl *Ssv0000001Impl) SV900006() error {
	Request := &models.SSV9000006I{
		MediaAgreementId: strconv.Itoa(int(time.Now().UnixNano())),
		MdsType:          impl.MediumType,
		MdsNm:            impl.MediumNm,
		Currency:         impl.SSV0000001I.Cur,
		CashtranFlag:     impl.SSV0000001I.CashTranFlag,
		ChannelNm:        "",
		UsgCod:           impl.SSV0000001I.UsgCod,
		AgreementId:      impl.AgreementId,
		AgreementType:    "10" + impl.SSV9000002O.DepositNature,
		DefaultAgreement: "Y",
		EffectiveFlag:    "Y",
		LastUpDatetime:   time.Now().Format("2006-01-02 15:04:05"),
		LastUpBn:         "",
		LastUpEm:         "",
	}
	rspBody, err := Request.PackRequest()
	if nil != err {
		return errors.New(err, constants.ERRCODE1)
	}
	_, err = impl.RequestSyncServiceWithDCN(impl.DcnId, constants.SV900006, rspBody);
	if err != nil{
		log.Errorf("SV900006, err:%v",err)
		return err
	}
	return nil
}

func (impl *Ssv0000001Impl) SV100015() error {
	Request := &models.SSV1000015I{
		Account:impl.SSV1000009O.AccountingId,
		DcnId : impl.SSV1000009O.DcnId,
	}
	SSV1000015IAResult := models.SSV1000015IAResult{
		InterestType:      "001",
		StrategyId:        impl.SSV0000001I.StrategyId,
		ItemId:            impl.SSV0000001I.ItemId,
		Rate:              impl.SSV0000001I.Rate,
		FloatValue:        impl.SSV0000001I.FloatValue,
		InterestMax:       impl.SSV0000001I.InterestMax,
		InterestMin:       impl.SSV0000001I.InterestMin,
		CalcType:          impl.SSV0000001I.CalcType,
		ExecType:          impl.SSV0000001I.ExecType,
		MonthDays:         impl.SSV0000001I.MonthDays,
		YearDays:          impl.SSV0000001I.YearDays,
		DecimalFlag:       impl.SSV0000001I.DecimalFlag,
		AccruePeriodType:  impl.SSV0000001I.AccruePeriodType,
		AccruePeriodValue: impl.SSV0000001I.AccruePeriodValue,
		SettlePeriodType:  impl.SSV0000001I.SettlePeriodType,
		SettlePeriodValue: impl.SSV0000001I.SettlePeriodValue,
	}
	Request.AResult = append(Request.AResult, SSV1000015IAResult)
	rspBody, err := Request.PackRequest()
	if nil != err {
		return errors.New(err, constants.ERRCODE1)
	}
	_, err = impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constants.SV100015, rspBody);
	if err != nil{
		log.Errorf("SV100015, err:%v",err)
		return err
	}

	return nil
}


func (impl *Ssv0000001Impl) SV100021() error {
	SSV1000021I := models.SSV1000021I{
		KeyType: constants.RSA256PRI,
	}
	rspBody, err := SSV1000021I.PackRequest()
	if nil != err {
		return errors.New(err, constants.ERRCODE1)
	}
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constants.SV100021, rspBody);
	if err != nil{
		log.Errorf("SV100021, err:%v",err)
		return err
	}
	Model := &models.SSV1000021O{}
	if err := Model.UnPackResponse(resBody); nil != err {
		return errors.New(err, constants.ERRCODE2)
	}
	impl.SSV1000021O = Model
	return nil
}

func HandResult (para *models.SSV1000021O, KeyVersion string) string{
	var PriKey = ""
	for _, para :=range para.Result {
		if KeyVersion == strconv.Itoa(para.KeyVersion) {
			PriKey = para.KeyValue
		}
	}
	return PriKey
}

func (impl *Ssv0000001Impl) GeneratePsw() error {
	//Pri_Key base64 Decode
	PriKey, _ := base64.StdEncoding.DecodeString(impl.PriKey)
	//AccPsw base64 Decode
	base64Psw, _ := base64.StdEncoding.DecodeString(impl.SSV0000001I.AccPsw)

	rsaInstance := commRsa.NewRSA(PriKey, nil)
	RsaPsw, err := rsaInstance.Decrypt(base64Psw)
	if nil != err {
		log.Errorf("VerifyPassword Decrypt fail，err：%v", err)
		return errors.New(err, constants.ERRCODE3)
	}
	//+salt
	byteRsq := util.GenerateKey(string(RsaPsw))
	impl.saltPsw = base64.StdEncoding.EncodeToString(byteRsq)

	return nil
}