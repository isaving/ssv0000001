package services

import (
	"errors"
	"fmt"
	"git.forms.io/isaving/models"
	jsoniter "github.com/json-iterator/go"
	. "reflect"
	"testing"
	"time"
)


func (this *Ssv0000001Impl) RequestSyncServiceWithDCN(dstDcn, serviceKey string, requestData []byte) (responseData []byte, err error) {
	return RequestService(serviceKey, requestData)
}

//这个要改成service的结构
func (this *Ssv0000001Impl) RequestSyncServiceElementKey(elementType, elementId, serviceKey string, requestData []byte) (responseData []byte, err error) {
	return RequestService(serviceKey, requestData)
}

func (this *Ssv0000001Impl) RequestServiceWithDCN(dstDcn, serviceKey string, requestData []byte, srcAppProps map[string]string) (responseData []byte, dstAppProps map[string]string, err error) {
	responseData = []byte(`{"Code":0,"Data":{"BS-LEGO":["C03101"]},"Message":"","errorCode":0,"errorMsg":"","response":{"BS-LEGO":["C03101"]}}`)
	return
}

func TestService1(t *testing.T) {
	fmt.Println(time.Now().UnixNano())
}

func TestService(t *testing.T) {
	runTest(Ssv0000001Impl{}, "TrySsv0000001")
	runTest(Ssv0000001Impl{}, "ConfirmSsv0000001")
	runTest(Ssv0000001Impl{}, "CancelSsv0000001")
}

var SrcAppProps = map[string]string{
	"TxUserId":   "1001",
	"TxDeptCode": "1001",
}

//输入请求
var request = []models.SSV0000001I{
	{
		PrductId		: "1",
		PrductNm        : 2,
		Cur             : "1",
		CashTranFlag    : "1",
		AccFlag         : "1",
		AppntDt         : "1",
		CstmrId         : "1",
		CstmrTyp        : "1",
		AccuntNme       : "1",
		UsgCod          : "1",
		WdrwlMthd       : "1",
		CshDpWdFlg      : "1",
		OpnAmt          : 1,
		AccCnclFlg      : "1",
		AutCnl          : "1",
		DytoCncl        : 22,
		SttmntFlg       : "1",
		WthdrwlMthd     : "1",
		OvrDrwFlg       : "1",
		CstmrCntctAdd   : "1",
		KeyVersion	    : "1",
		CstmrCntctPh    : "1",
		CstmrCntctEm    : "1",
		BnCrtAcc        : "1",
		AccPsw          : "r82DlMrx8MESC2iNugt5vYSsubk/aMNGg5F0L5zPqaq8KbSJej9AtVE8leUnX+JH6SM/LyjuEtzintpRuhAlAtWv1GDzSTPuy3KpX81mY4zBkVJ+tg2trOApthmfjktrfdqewC3RXOEe35mAXU6SJTsWd1t6JtHmw3p1JBr+LSkT487ypEI3rYVZeBYbrVufjeuQGLJmo+J6FxbUZ//fsG2FSbmwuTdUMBkmiVxg25JDGIf0eMh5T7TsuZG3CHYbJbLV6YtZJPlsLNiMiZBfcFy5fxE5QFapfP5oIzNmzhWc5ogUoLeTiP6whb/9SpiK2OtACOO+fp8uv0/zV3+ysQ==",
		BnAcc           : "1",
		AccOpnDt        : "1",
		AccOpnEm        : "1",
		ChkDTrnFlag     : "1",
		ChkDTrnCode     : "1",
		RequrstId       : "1",

		InterestType		: "1",
		StrategyId          : "1",
		ItemId              : "1",
		Rate                : 1,
		FloatValue          : 2,
		InterestMax         : 2,
		InterestMin         : 2,
		CalcType            : "1",
		ExecType            : "1",
		MonthDays           : "1",
		YearDays            : "1",
		DecimalFlag         : "1",
		AccruePeriodType    : "1",
		AccruePeriodValue   : 2,
		SettlePeriodType    : "1",
		SettlePeriodValue   : 2,
	},
	{
		PrductId		: "331",
		PrductNm        : 2,
		Cur             : "1",
		CashTranFlag    : "1",
		AccFlag         : "1",
		AppntDt         : "1",
		CstmrId         : "1",
		CstmrTyp        : "1",
		AccuntNme       : "1",
		UsgCod          : "1",
		WdrwlMthd       : "1",
		AccPsw          : "r82DlMrx8MESC2iNugt5vYSsubk/aMNGg5F0L5zPqaq8KbSJej9AtVE8leUnX+JH6SM/LyjuEtzintpRuhAlAtWv1GDzSTPuy3KpX81mY4zBkVJ+tg2trOApthmfjktrfdqewC3RXOEe35mAXU6SJTsWd1t6JtHmw3p1JBr+LSkT487ypEI3rYVZeBYbrVufjeuQGLJmo+J6FxbUZ//fsG2FSbmwuTdUMBkmiVxg25JDGIf0eMh5T7TsuZG3CHYbJbLV6YtZJPlsLNiMiZBfcFy5fxE5QFapfP5oIzNmzhWc5ogUoLeTiP6whb/9SpiK2OtACOO+fp8uv0/zV3+ysQ==",
		CshDpWdFlg      : "1",
		OpnAmt          : 1,
		AccCnclFlg      : "1",
		AutCnl          : "1",
		DytoCncl        : 22,
		KeyVersion	    : "1",
		SttmntFlg       : "1",
		WthdrwlMthd     : "1",
		OvrDrwFlg       : "1",
		CstmrCntctAdd   : "1",
		CstmrCntctPh    : "1",
		CstmrCntctEm    : "1",
		BnCrtAcc        : "1",
		BnAcc           : "1",
		AccOpnDt        : "1",
		AccOpnEm        : "1",
		ChkDTrnFlag     : "1",
		ChkDTrnCode     : "1",
		RequrstId       : "1",
		InterestType		: "1",
		StrategyId          : "1",
		ItemId              : "1",
		Rate                : 1,
		FloatValue          : 2,
		InterestMax         : 2,
		InterestMin         : 2,
		CalcType            : "1",
		ExecType            : "1",
		MonthDays           : "1",
		YearDays            : "1",
		DecimalFlag         : "1",
		AccruePeriodType    : "1",
		AccruePeriodValue   : 2,
		SettlePeriodType    : "1",
		SettlePeriodValue   : 2,
	},
}


//使用反引号可以直接换行 还不会被带双引号影响
var response = map[string]interface{}{
	//这个key 是你调用RequestSyncServiceElementKey()这个函数传的serviceKey的字符串 不一定是topic 好好看看自己的代码传的字符串是啥
	//可以直接粘贴报文 也可以写一个返回结构体
	"SV900002": `{"errorCode":"0","errorMsg":"success","response":{"PrductStatus":"1","ProductNameCh":"存款","ProductNameEn":"存款","Currency":"CNY","OffShoreOnshoreSign":"","TermFlag":"1","PrductBuyTime":10,"BlncRflctsDrcton":"5","AmtOpenMin":50,"DepositNature":"1","ApprvlMark":"1","WdrwlMthd":"柜","TrnWdrwmThd":"","AppntDtFlg":"1","DepcreFlag":"1","SttmntFlg":"1","SttmntType":"1","SttmntPeriod":"1","SttmntFrequency":1,"SttmntDate":1,"SlpTrnChange":"1","StpTrnAut":"1","StpAmtMin":1,"StpGraceDate":1,"StpNotDate":1,"StpIntFlag":"1","StpUnclaimedDay":1,"CancelAutoFlag":"1","CancelAutoDay":1,"CancelRepnFlag":"1","CashAccessFlag":"","AmtDepositMin":0,"ProductNum":0,"DepositDayMin":0,"DepositDayMax":0,"DepositPeriodUnit":"","DepositPeriod":0,"DepositAgainFlag":"","AdvanceWithdrawNum":0,"MaturityWithdrawNum":0,"OverdueWithdrawNum":0,"AdvanceWithdrawGraceNum":0,"WithdrawAmtMin":0,"KeepAmtMin":0,"MaturityFlag":"","ConDepositNum":0,"AmtConMin":0,"BalprocessWay":"","BalConMatWay":"","PrinConMatWay":"","HolidayFlag":""}}`,
	"SV100008": `{"errorCode":"0","errorMsg":"success","response":{"ContractCount":1,"ContractType":"1"}}`,
	"SV100009": `{"errorCode":"0","errorMsg":"success","response":{"AccountingId":"1","ContractType":"1"}}`,
	"SV900001": `{"errorCode":"0","errorMsg":"success","response":{"AgreementID":"1","ContractType":"1"}}`,
	"SV100010": `{"errorCode":"0","errorMsg":"success","response":{"AccountingId":"1","ContractType":"1"}}`,
	"SV900006": `{"errorCode":"0","errorMsg":"success","response":{"State":"OK"}}`,
	"SV100015": `{"errorCode":"0","errorMsg":"success","response":{"State":"OK"}}`,
	"SV100021": `{"errorCode":"0","errorMsg":"success","response":{"Result":[{"KeyType":"RSA256-PRI","KeyVersion":1,"KeyValue":"LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFb2dJQkFBS0NBUUVBOG5OWTV1QjJQbUhNeENreUUzc2FRdkpMRkcyNFRocmJlN043VnFKMEJ0aWhNYWlJCmp3M0lKbWVBeFFuQUV3YzFTcTY1QXBiOGhNZS9lL3N2Y3ZrQ1hDYjJ1Q3JwZi9kRHBDa3FIS0xlT2V1SUZEZ3UKTWNLb21Kcmo0RFUyK1ZKaVFHWEJBREwyZ3dZNldYQVRCN1RERW1HZU1ZNEE1VWZDWHBvWFRmUWRXVHlxeHIrYQo1eFAwd0k0b0o3NTROSEhudjgyL21tS1lXRktYU3hMVjJSbVlsMmFwTWwxTHpaQ0tCNjlpQlBoWnk1Vm9ycXliClpCVHEwUlNOUGJ6SVNOSC9QMW84djlQNTdtZ2dGaTVtamNLdHYyTmtLajFVWmVEYzJLL09pd1RiUTdJcmRjdEMKL2o2d1F0OFB4bjFTWldwT2tEbGNOb05rVzNOaUl5cXd2anc4dHdJREFRQUJBb0lCQUVzclJoSDIzOUpCZ2pvRApSN3BRZlhqcUpDc0dSWkU1dEhqcTBiK3ZpR2VpL0g4bjhJYmh6LzlRTWRWbFMzN2F5a1FJWGVZV3A5NEhEREJGClZaWEE3R0ZqMXpTTVlXN0NXOTltdjk2M0JFMVM0QkxjeEQyN1M0aGM1Vy9iTDlzbkZyMCtqeE1mdWR3dnJ5R1IKcE9UTHYzdFFkcnFJZTNDYXlnR0lwdGNOYTVnQm83Z1AwdUdvSmVyT1g4dUlna0p3OXRBWmxJMmNYSlVNdTUzVQp5YmpybkdjKzlNcG5aT2x4ZjNHSDc4cDNyZEVYb3ppa0dKbWpYU3VRNGdqTkE5THF4ZHdvSDEzeGU2UWtsYzVrClh1b0RnZ0N6Zkg1Njc4bUIyWDNuSHVoaEx4NWxuV1JoaWJYN3plWERNUmoyUi9xdnNiZ0hiU0QzbnpsUS9mb3oKcVA4cnBtRUNnWUVBK3ZZdnI3RS93a0d6S2JyaWpyaGpGZzNhcks5eGZPdXBWeWxoYy9paDQ4WVJDN0ZDYldLbQo0SWhyT2FoSEtOTzBnRGhmd1JvNEpnb0tSNGpjVGlNaVc4ekJ4czFyZXI0NUpCNmF6MmJvbHhEbDNXTDNDVUx4CjlLbDk2dUdhaGJ3cmVnY3hrTkorSVllRS92clFJc3A5YjRxQ0J0QVdjTWttLzd6eG03YUd1eWNDZ1lFQTkxRnIKR3VkNW5aQVRIWEg3cHRMY3pEYTJtbFJ5Z20zWGF1a3VXNTVvRjQ3b1lZMnpvTlhtRDJrVEZKQmEyK285bTUzMQpQNTBIZE9hRENnZGpLL2lTOE02QmZHOUJCMzFKYmljTUtORXNUMHlRZkU4Y3NDWm5EUzJqTWVWQ2VoOVBhVUNOClB6a21ySjlLNlNUOGdxSWZtamp4S1VYMkE1a21SL29HNWN4bHEvRUNnWUFtSjcvZ1NtYVROa0h6R2tMUHlDRG8KZlhxOXZuN1dxMDIzdnNiZVJ2TDJHbXNGQ0JJOVp6NVA3TzNBZW1FM1I5aXRsZVNaQzA5MkdSaXUvNVBmSFdCZgo3S1daZ0NuVEk1QzBlU0s0WGNFSEZyNmNCaXZnOEppd3hnUXE3ZG44Y3p0NDloY0hHYm1yaDFzbEdTVUQ1UjFsCkUvM280MEpHSzQ4T1dyK3hHUE1yQ1FLQmdCa3ZqMnF6Kyt5UnlZQ01KUVorSHNJbEtEOWJMalllSFh2anR6YloKVU54bFQwRHYvMFQzdzIvNlVWa2N3a1AyL01NbU9OMTdqbERYVUt6cXg2eW9xYjRnS1l1VWdOalVaMi95UmdpNgpGSHNXM3ZmcHpwQVp1UkRjZnV3TDN0TTA4UTlVNVkxMHNuMk4wMEVlNVNBZ2hudGNKU01BM3ViSzU5MFlseTg3CnUzYmhBb0dBWGp5ajBaZmtTbll1NWgzeXpEMmgyb0M1b3FNYjdSYjVnZnNRcEpYZE9NK0tEZCtCeFdDMWpSTmcKM0tTU1pBODgrek9tL2ZYSTB6ellXc2MzdjNYS3pKcFNhZk50SU0wbFFkdTEzTmY4K1UxRitpejcralp4WDJ4cApIUTFONXVndXNmcnpPaUFKNVBRS1kxM2lzQTFDUWNGV0RrYXJacFJLRm5YS3Z2emoyWGc9Ci0tLS0tRU5EIFJTQSBQUklWQVRFIEtFWS0tLS0tCg==","EffectTime":"2020-12-17 17:46:18","DeffectTime":"2020-12-24 17:46:18"},{"KeyType":"RSA256-PRI","KeyVersion":2,"KeyValue":"LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFcFFJQkFBS0NBUUVBemJYb1RJbFUxTndlTitEMjh2VVIwVUcxNHdYN0dVYTJwc24xbUpOaUJuNVZMeEpmClVtaGpSZ2hxOFNPZWZCNXpab2ZHVGhwUXhKTXRvdFEra2tycG5SRjMrYkhIVnNWeWc0U3krTVFBQ09Wdm1tSjkKL0x2RytCOXB3RCtkVVpVc0tXbjJGbDA4NU1NZnVrZGdpYjZJcFdmR3loc2g3L1NvTzRGYzR2bXV5alliYTk2UwpkcWxmTTJ3SU1JcllhQU14Y0RjdGVhRFFGWXNqN1A1V1g3YzQ2Sm9UZ2xoYWgvRnA3Vnc1OG1jZ3VPeElwc21wCnZYUlVXMUxpVkFrYmFlZ0x2d2ZmTm5VcEYxREcveVF1ZGhEVGErSHpJK0VZajV2U2dlUkpYYi9HUHh2WUl6c0MKZnduMWJ2ZTVRR0FGdFg2YnN5SHN0RnpsNENDOHFuaUxCTDZmZlFJREFRQUJBb0lCQUdWSCtkNnRBdnBtZ2U1TApNZXIvSkUyZUI3cUYrRkpDa2ExZTVDL0I2Tkovb3RzeE5ZTnlaK1pVUUtzSE9ZR3MwRzZiek5RcGVyaStLZ3J3CmF2cEdMZTBxYTZLaG1CNnR5aXJjeSsxVk94c21NNk9VZUtsVm5xb05kVmkxT1hTd2VIVFBlZDBhL044NC9zY04KVzRlV0t0c2RWQyt2Mmk2MWk1cjRuMDRWVnF5eHdtaFdJRlI2bm5WNGhVRXJEL3krTmFURUtTN2pmWmw3Sm83Nwpra29iallnU2ZhUWFOYnlucmI3bGN2UG84dUVJRk92akhHYTNicXJBWVBQVVMxUkNhZEVoaHR0TFNhN1FVTEd0CkFhcS9pZEk1QWRuTUVPN2RHeWhCQnIrUW9qTnZEWnVoRzZYdkhHN09uYzcvRk53dUppQkJKbmZrejlRTlZtKysKelVZSUtZRUNnWUVBK09lV2lkOUl1dWEwTm1jTHNYS09sZnJ5YktnTi9wcWRwa1FZR0ltYWtFWkc5K2FHNXkxeQpJa2NjMDBYTUlLK3hWTTNRd05lMWd2WUdvZXc3Nnh3VGVpMituVHRmblBqMG4yWnVwc2dwNWJPQjdsd21aRS9KCmNIWlNQWmVsUGJTUmdUSEpwWlhuVm5WSksyYzd1bUR4YUN3cTFaVnJQeXAwWEYvTE5nUlp4eDBDZ1lFQTA1TWEKL1BLWHhDOVczdDlWLzVHcnBoOE5zV2txMFhEVWQ2TVBIMDA5UHVjOHl0WjlaS3RLcnZ1TTVFZURoSXZza3lxZQphaDNjeS9ldkgvNkdvOXIvbi9CekM1Qno1WFBRb24yK2xuSXd6YnVuTVRZR2s4TE05c3NQTFlzWTBFY3dOaGQrCjN6c2lDLzZOaFZLTHNsYzNuZTZpVGREdncrSWlEL0lINDJnYjYrRUNnWUVBcXh5YWt3SGJRQkhtQmhobEk0STcKbjhPNjhSTy9PeThUL2YrR0NTaGdCNXNvb3JmcU9mYzJKWFo1ekl5SlBsZEtNNkNpU0ZDQ3lodFVoL0NkYUk0TwpuY25nNXJxd2ludGp3RWxzL0Y5UzNrUy9xQnZWa05TQkNUMXVZRVZtSmJ1QzVDeFVFZSt4Ny9BRmN6OHZBbnd1CnVZelRqYTJBNHhYT2xNcU9LdjVtc2NrQ2dZRUFsKzJad3F3ZHFXWnBGakFBSEQ2cHFPZkJ1SFVScDBaRGZBbWUKY1ZiUXRSdnR2cVpaY3ZIQjluZHlpL2FDamFyQTNra0xhcDQ1RUVxeStlR1hJNUMrWVVhVUJtamJvSmdTNkt3dApNUEdjOUE5ZGx1djV1eDkyZVg0RlR5b0VUb1BnTGVlZlZvT3JCanZydWlJMWFJSlMwTmtzdVg1Z3ZpUGpXOW5zCktqTjNZV0VDZ1lFQWs3aGlHbXk0b3MxVEhCcjJpckFabDBPN3BpMmdJdzd3ckF2NjROZEEvaTNCQTRZY0duS2QKVXl1dng0Y1JrdTh6cWxNdVdOVFpjYnQxQ3k1SjluOC9nNGVSOHBXdnNxZmJhalRubjdvMzdXN01IbEN5Q3JUbgpOUm0vQWEyVUVkY2ZWTGtQTkNlQVpwU0lra2tDbnZMcHl5VmdiZEFNQlhNRWNtSmF0NVR4WmZ3PQotLS0tLUVORCBSU0EgUFJJVkFURSBLRVktLS0tLQo=","EffectTime":"2020-12-17 17:48:19","DeffectTime":"2020-12-24 17:48:19"}]}}`,

	"DlsupCreateDTS": `{"Code":"0","Message":"","Data":{"DcnType":"1","DcnId":"1"}}`,

}

//func (this *Ssv0000001Impl)RequestServiceWithDCN(dstDcn, serviceKey string,requestData []byte, srcAppProps map[string]string) (responseData []byte, dstAppProps map[string]string, err error) {
//	if serviceKey == "DlsuDcnLists" {
//
//	}
//}

var errServiceKey []string

//初始化
func init() {
	for serviceKey := range response {
		errServiceKey = append(errServiceKey, serviceKey)
	}
}

var currentIndex = 0
var passed bool

//排列组合返回报文
func RequestService(serviceKey string, reqBytes []byte) (responseData []byte, err error) {

	if serviceKey == errServiceKey[currentIndex] && passed {
		currentIndex++
		if currentIndex >= len(errServiceKey) {
			currentIndex = 0
		}
		return nil, errors.New("error")
	}
	//这里进行条件控制
	switch response[serviceKey].(type) {
	case string:
		responseData = []byte(response[serviceKey].(string))
	default:
		responseData = getByte(response[serviceKey])
	}
	return
}

func getByte(v interface{}) (bt []byte) {
	bt, _ = jsoniter.Marshal(struct {
		Form [1]struct{ FormData interface{} }
	}{Form: [1]struct{ FormData interface{} }{{v}}})
	return
}

func runTest(serviceStruct interface{}, method string) {
	param := make([]Value, 1)
	for _, req := range request {
		param[0] = ValueOf(&req)
		passed = false
		for i := 0; i < len(response); i++ {
			callMethod(serviceStruct, method, param)
			passed = true
		}
	}
}

func callMethod(serviceStruct interface{}, method string, params []Value) {
	service := New(TypeOf(serviceStruct))
	service.Elem().FieldByName("SrcAppProps").Set(ValueOf(SrcAppProps))
	ret := service.MethodByName(method).Call(params)
	fmt.Printf(" 返回 [ %v ] \n 错误 [ %v ]\n\n", ret[0], ret[1])
}


