#自动生成结构体
#bee api ProjectName -tables="TableName" -conn="root:Yy123456@tcp(172.21.67.188:3306)/loan"
#bee api ProjectName -conn="root:Yy123456@tcp(172.21.67.188:3306)/loan"

#加依赖
#govendor init
#govendor add +e

#上传git
#git init
#git remote add origin  http://172.22.80.85/legobankil/$appid.git
#git checkout  dev
#git push origin :dev
#git push origin :dev-0.0.1

#git push origin develop:master -f
#就可以把本地的develop分支强制(-f)推送到远程master

#git reset --hard develop // 将本地的旧分支 master 重置成 develop
#git push origin master --force // 再推送到远程仓库
git add .
git commit -am "first commit"
git push -u dev

#git checkout dev

#. 查看所有标签
#git tag

#.本地打新标签
#git tag v1.1.0

#. 附注标签
#git tag -a <tag name> -m <message>

#. 本地推送到远程
#git push origin <tag name>  // 推送一个标签到远程
#or
#git push origin --tags   // 推送全部未推送的本地标签

#. 本地删除标签
#git tag -d <tag name>

#. 并远程删除标签
#git push origin :refs/tags/<tag name>   // 本地tag删除了，在执行该句，删除远程tag

#编研
#CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build .