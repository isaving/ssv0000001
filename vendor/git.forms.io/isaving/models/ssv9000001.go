//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV9000001I struct {
	AgreementId      	string		//合约号8+12+1 8位银行号：乐高默认是62822222 12位编码：顺序号
	AgreementType       string      //合约类型
	Currency     		string      //币种
	CashtranFlag        string      //钞汇标识			C-钞户;T-汇户
	AgreementStatus     string      //合约状态            0-正常;1-销户计息;2-结清;3-解约-
	FreezeType          string      //冻结状态            0-正常;1-合约冻结;2-金额冻结
	TmpryPrhibition     string      //暂禁状态 0-正常，1-暂禁
	TermFlag            string      //是否分期标志        1-不分期；2-分期
	DepcreFlag          string      //借贷记控制标志      N-正常;D-只借记;C-只贷记;F-不允许借贷记
	AccFlag             string      //是否核算标识        0-是;1-否;
	AccAcount           string      //核算账号
	BegnTxDt            string      //起息日期
	AppntDtFlg          string      //是否指定到期日      0-是;1-否;2-未知
	AppntDt             string      //到期日期
	PrductId            string      //产品号
	PrductNm            int        //产品顺序号
	CstmrId             string      //客户编号
	CstmrTyp            string      //客户类型            1-个人客户;2-法人客户;3-联名客户;4-集团
	AccuntNme           string      //账户名称
	UsgCod              string      //用途代码
	WdrwlMthd           string      //支取方式            0-密码;1-印鉴;2-签字、3-指纹;4-密码+印
	AccPsw              string      //账户密码
	PswWrongTime        int         //密码错误次数
	TrnWdrwmThd         string      //通兑方式            0-非通存通兑;1-分行通兑;2-全行通兑
	OpnAmt              float64     //开户金额
	AccCnclFlg          string      //销户启用标识        Y-启用;N-不启用
	AutCnl              string      //是否允许自动销户    0-是;1-否;
	DytoCncl            int         //自动销户宽限天数
	SttmntFlg           string      //对账单标识          Y-出对账单;N-不出对账单
	WthdrwlMthd         string      //取款方式            1-存折;2-密码;3-证件;4-印鉴
	OvrDrwFlg           string      //透支标识            N-不可透支;Y-可透支除金额冻结
	CstmrCntctAdd       string      //客户联系地址
	CstmrCntctPh        string      //客户联系电话
	CstmrCntctEm        string      //邮箱地址
	BnCrtAcc            string      //开户行所
	BnAcc               string      //账务行所
	AccOpnDt            string      //开户日期
	AccOpnEm            string      //开户柜员
	AccCanclDt          string      //销户日期
	AccCanclEm          string      //销户柜员
	AccCanclResn        string      //销户原因
	LastUpDatetime      string      //最后更新日期
	LastUpBn            string      //最后更新行所
	LastUpEm            string      //最后更新柜员

	GlobalBizSeqNo		string	//全局流水号
	SrcBizSeqNo			string	//服务流水号
	DepositNature		string	//账户性质 00-基本账户;01-一般账户
	MediumType			string	//开户介质类型
	MediumNm			string	//开户介质号码
}

type SSV9000001O struct {
	AgreementID		string	//合约号

}

// @Desc Build request message
func (o *SSV9000001I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV9000001I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV9000001O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV9000001O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV9000001I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV9000001I) GetServiceKey() string {
	return "ssv9000001"
}
