//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV1000010I struct {
	CustId		string	`validate:"required"`	//客户号
	Account		string	`validate:"required"`	//合约号
	ContactType	string	`validate:"required"`	//合约号
	AListContact	[]SSV1000010IAListContact
	AListAddr		[]SSV1000010IAListAddr
	ObjPublic		SSV1000010ObjPublic
}

type SSV1000010IAListContact struct {
	ContactType	string	//联系信息类型	//01-家庭电话，02-办公电话，03-手机，04-Email，05-传真，06-微信，07-QQ，09手机，10-其他
	Contact		string	//联系方式
	Name		string	//称谓
	UseType 	string 	//使用类型
}

type SSV1000010IAListAddr struct {
	AddrType	string  //地址类别 1:家庭住址 2：办公住址 3：证件地址 4：注册地址 5：户籍地址 9：其他地址
	NationCode	string  //国别
	Province	string  //省/州
	City		string  //城市
	District	string  //区县
	Addr		string	//详细地址
	UseType 	string 	//使用类型
}

type SSV1000010ObjPublic struct {
	BranchNo	string	//机构编号
	TellerId	string	//员工编号

}


type SSV1000010O struct {
}

// @Desc Build request message
func (o *SSV1000010I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV1000010I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV1000010O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV1000010O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV1000010I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV1000010I) GetServiceKey() string {
	return "ssv1000010"
}
