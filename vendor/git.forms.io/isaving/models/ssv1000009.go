//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV1000009I struct {
	CustId         string  `json:"CustId" validate:"required"`
	Account        string  `json:"Account" validate:"required"`
	Amount         float64 `json:"Amount"`
	AlloWithdTimes int     `json:"AlloWithdTimes" validate:"required"`
	Currency       string  `json:"Currency" validate:"required"`
	ObjPublic      string  `json:"ObjPublic"`
	BranchNo       string  `json:"BranchNo" validate:"required"`
	TellerId       string  `json:"TellerId" validate:"required"`
}

type SSV1000009O struct {
	AccountingId	string	//核算账号
	DcnId			string	//DCN分片
}

// @Desc Build request message
func (o *SSV1000009I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV1000009I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV1000009O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV1000009O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV1000009I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV1000009I) GetServiceKey() string {
	return "ssv1000009"
}
