//Version: v0.01
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV9000002I struct {
	PrductId      string `valid:"Required;MaxSize(30)" description:"product code"`                      //产品号
	PrductNm      int    `valid:"Required;MaxSize(5)" description:"Product serial number"`              //产品顺序号
	AgreementType string `valid:"Required;MaxSize(5)" description:"Contract type"`                      //合约类型	10001-个人活期账户;10002-个人定期账户;10003-个人活期保证金账户;10004-个人定期保证金账户;10005-对公活期账户;10006-对公定期账户;10007-对公活期保证金账户;10008-对公定期保证金账户;10009-内部账户;20001-个人贷款账户;20002-对公贷款账户;30001-协议存款;30002-保证金;30003-同业存款;30004-智能通知存款;30005-法人账户透支;30006-集团账户;30007-协定存款;30008-收息账号;30009-活期存款转定期约定;30010-网银开户签约;30011-贷款;30012-贷款收费;30013-普通定期存款;30014-活期保证金存款;30015-定期保证金存款;30016-普通活期存款;40001-客户对账单;-
	CUR           string `valid:"Required;MaxSize(3)" description:"Currency"`                           //币种	156-人民币;344-香港元;840-美元;392-日元;826-英镑;756-瑞士法郎;280-德国马克;250-法国法郎;702-新加坡元;528-荷兰盾;208-丹麦克郎;578-挪威克郎;040-奥地利先令;056-比利时法郎;380-意大利里拉;124-加元;036-澳大利亚元;458-马来西亚林吉特;978-欧元;446-澳门元;608-菲律宾比索;554-新西兰元;000-未知币种;004-阿富汗尼;008-列克;012-阿尔及利亚第纳尔;020-安道尔比赛塔;031-阿塞拜疆马纳特;032-阿根廷比索;044-巴哈马元;048-巴林第纳尔;050-塔卡;051-亚美尼亚达姆;052-巴巴多斯元;060-百慕大元;064-努尔特鲁姆;068-玻璃瓦诺;072-普拉;084-伯利兹元;090-所罗门群岛元;096-文莱元;024-新克瓦查;100-列弗;104-缅元;108-布隆迪法郎;116-瑞尔;132-佛得角埃斯库多;136-开曼群岛元;144-斯里兰卡卢比;152-智利比索;170-哥伦比亚比索;174-科摩罗法郎;188-哥斯达黎加科郎;191-克罗地亚库纳;192-古巴比索;196-塞浦路斯镑;203-捷克克郎;214-多米尼加比索;222-萨尔瓦多科郎;230-埃塞俄比亚比尔;233-克罗姆;238-福克兰群岛镑;242-斐济元;246-马克;262-吉布提法郎;270-达拉西;288-塞地;292-直布罗陀镑;300-德拉克马;320-格查尔;324-几内亚法郎;328-圭亚那元;332-古德;340-伦皮拉;348-福林;352-冰岛克郎;356-印度卢比;360-卢比;364-伊朗里亚尔;368-伊拉克第纳尔;372-爱尔兰镑;376-锡克尔;388-牙买加元;398-坚戈;400-约旦第纳尔;404-肯尼亚先令;408-北朝鲜圆;410-圆;414-科威特第纳尔;417-索姆;418-基普;422-黎巴嫩镑;426-罗提;428-拉托维亚拉特;430-利比里亚元;434-利比亚第纳尔;440-立陶宛;442-卢森堡法郎;450-马尔加什法郎;454-克瓦查（马拉维）;462-卢菲亚;470-马尔他里拉;478-乌吉亚;480-毛里求斯卢比;484-墨西哥比索;496-图格里克;498-摩尔瓦多列伊;504-摩洛哥迪拉姆;508-麦梯卡尔;512-阿曼里亚尔;516-纳米比亚元;524-尼泊尔卢比;532-荷属安的列斯盾;533-阿鲁巴盾;548-瓦图;558-金科多巴;566-奈拉;586-巴基斯坦卢比;590-巴波亚;598-基那;600-瓜拉尼;604-索尔;620-葡萄牙埃斯库多;624-几内亚比绍比索;626-东帝汶埃斯库多;634-卡塔尔里亚尔;642-列伊;646-卢旺达法郎;654-圣赫勒拿镑;678-多布拉;682-沙特里亚尔;690-塞舌尔卢比;694-利昂;703-斯洛伐克克郎;704-盾;705-托拉尔;706-索马里先令;710-兰特;716-津巴布韦元;724-西班牙比赛塔;736-苏丹第纳尔;740-苏里南盾;748-里兰吉尼;752-瑞典克郎;760-叙利亚镑;764-泰国铢;776-邦加;780-特立尼达和多巴哥元;784-UAE迪拉姆;788-突尼斯第纳尔;792-土耳其里拉;795-马纳特;800-乌干达先令;807-第纳尔;810-俄罗斯卢布;818-埃及镑;834-坦桑尼亚先令;858-乌拉圭比索;860-乌兹别克斯坦苏姆;862-博利瓦;882-塔拉;886-也门里亚尔;894-克瓦查（赞比亚）;901-新台湾元;950-CFA法郎BEAC;951-东加勒比元;952-CFA法郎BCEAO;953-CFP法郎;955-欧洲货币合成单位（EURCO）;956-欧洲货币（单位EMU.-6）;957-欧洲账户9单位（E.U.A.-9）;958-欧洲账户17单位（E.U.A.-17）;959-黄金;960-特别提款权;961-银;962-铂白金;963-为测试特别保留的代码;964-钯;972-索莫尼;974-白俄罗斯卢布;975-保加利亚列弗;976-刚果法郎;977-可自由兑换标记;979-墨西哥发展单位;980-格里夫纳;981-里拉;984-Mvdol;985-兹罗提;986-巴西瑞尔;990-发展单位;991-特别清算货币;992-黄金法郎（实体）;993-UIC法郎;994-黄金法郎（测试）;995-无通用货币;996-债券市场单位;997-美元次日;998-美元同日;999-记账瑞士法郎;232-纳克法;891-南斯拉夫第纳尔;-
	PeriodFlag    string `valid:"MaxSize(1)" description:"Whether to install the mark"`                 //是否分期标志	1-不分期；2-分期
	DpstUn        string `json:"DpstUn,omitempty" valid:"MaxSize(1)" description:"Deposit period unit"` //存期单位	如果PeriodFlag=2，则本栏位必填	I-分钟;O-小时;D-日;W-周;M-月;S-季;H-半年;Y-年;-
	Mtrty         int    `json:"Mtrty,omitempty" valid:"MaxSize(6)" description:"Deposit period"`       //存期	INTEGER	6	如果PeriodFlag=2，则本栏位必填

}

type SSV9000002O struct {
	PrductStatus       string  `valid:"MaxSize(1)" description:"prduct status"`                                              //产品状态	1-正常(能被客户购买与使用);2-不开放(不能被客户购买，但能被使用（如有存量数据）);2-作废(不能被客户购买或使用);3-衍生产品（基于其他产品才能签约购买和使用）;-
	ProductNameCh       string  `valid:"MaxSize(80)" description:"Product Chinese name"`                                       //产品中文名称
	ProductNameEn       string  `valid:"MaxSize(80)" description:"Product English name"`                                       //产品英文名称
	Currency            string  `valid:"MaxSize(3)" description:"Currency"`                                                    //币种		156-人民币;344-香港元;840-美元;392-日元;826-英镑;756-瑞士法郎;280-德国马克;250-法国法郎;702-新加坡元;528-荷兰盾;208-丹麦克郎;578-挪威克郎;040-奥地利先令;056-比利时法郎;380-意大利里拉;124-加元;036-澳大利亚元;458-马来西亚林吉特;978-欧元;446-澳门元;608-菲律宾比索;554-新西兰元;000-未知币种;004-阿富汗尼;008-列克;012-阿尔及利亚第纳尔;020-安道尔比赛塔;031-阿塞拜疆马纳特;032-阿根廷比索;044-巴哈马元;048-巴林第纳尔;050-塔卡;051-亚美尼亚达姆;052-巴巴多斯元;060-百慕大元;064-努尔特鲁姆;068-玻璃瓦诺;072-普拉;084-伯利兹元;090-所罗门群岛元;096-文莱元;024-新克瓦查;100-列弗;104-缅元;108-布隆迪法郎;116-瑞尔;132-佛得角埃斯库多;136-开曼群岛元;144-斯里兰卡卢比;152-智利比索;170-哥伦比亚比索;174-科摩罗法郎;188-哥斯达黎加科郎;191-克罗地亚库纳;192-古巴比索;196-塞浦路斯镑;203-捷克克郎;214-多米尼加比索;222-萨尔瓦多科郎;230-埃塞俄比亚比尔;233-克罗姆;238-福克兰群岛镑;242-斐济元;246-马克;262-吉布提法郎;270-达拉西;288-塞地;292-直布罗陀镑;300-德拉克马;320-格查尔;324-几内亚法郎;328-圭亚那元;332-古德;340-伦皮拉;348-福林;352-冰岛克郎;356-印度卢比;360-卢比;364-伊朗里亚尔;368-伊拉克第纳尔;372-爱尔兰镑;376-锡克尔;388-牙买加元;398-坚戈;400-约旦第纳尔;404-肯尼亚先令;408-北朝鲜圆;410-圆;414-科威特第纳尔;417-索姆;418-基普;422-黎巴嫩镑;426-罗提;428-拉托维亚拉特;430-利比里亚元;434-利比亚第纳尔;440-立陶宛;442-卢森堡法郎;450-马尔加什法郎;454-克瓦查（马拉维）;462-卢菲亚;470-马尔他里拉;478-乌吉亚;480-毛里求斯卢比;484-墨西哥比索;496-图格里克;498-摩尔瓦多列伊;504-摩洛哥迪拉姆;508-麦梯卡尔;512-阿曼里亚尔;516-纳米比亚元;524-尼泊尔卢比;532-荷属安的列斯盾;533-阿鲁巴盾;548-瓦图;558-金科多巴;566-奈拉;586-巴基斯坦卢比;590-巴波亚;598-基那;600-瓜拉尼;604-索尔;620-葡萄牙埃斯库多;624-几内亚比绍比索;626-东帝汶埃斯库多;634-卡塔尔里亚尔;642-列伊;646-卢旺达法郎;654-圣赫勒拿镑;678-多布拉;682-沙特里亚尔;690-塞舌尔卢比;694-利昂;703-斯洛伐克克郎;704-盾;705-托拉尔;706-索马里先令;710-兰特;716-津巴布韦元;724-西班牙比赛塔;736-苏丹第纳尔;740-苏里南盾;748-里兰吉尼;752-瑞典克郎;760-叙利亚镑;764-泰国铢;776-邦加;780-特立尼达和多巴哥元;784-UAE迪拉姆;788-突尼斯第纳尔;792-土耳其里拉;795-马纳特;800-乌干达先令;807-第纳尔;810-俄罗斯卢布;818-埃及镑;834-坦桑尼亚先令;858-乌拉圭比索;860-乌兹别克斯坦苏姆;862-博利瓦;882-塔拉;886-也门里亚尔;894-克瓦查（赞比亚）;901-新台湾元;950-CFA法郎BEAC;951-东加勒比元;952-CFA法郎BCEAO;953-CFP法郎;955-欧洲货币合成单位（EURCO）;956-欧洲货币（单位EMU.-6）;957-欧洲账户9单位（E.U.A.-9）;958-欧洲账户17单位（E.U.A.-17）;959-黄金;960-特别提款权;961-银;962-铂白金;963-为测试特别保留的代码;964-钯;972-索莫尼;974-白俄罗斯卢布;975-保加利亚列弗;976-刚果法郎;977-可自由兑换标记;979-墨西哥发展单位;980-格里夫纳;981-里拉;984-Mvdol;985-兹罗提;986-巴西瑞尔;990-发展单位;991-特别清算货币;992-黄金法郎（实体）;993-UIC法郎;994-黄金法郎（测试）;995-无通用货币;996-债券市场单位;997-美元次日;998-美元同日;999-记账瑞士法郎;232-纳克法;891-南斯拉夫第纳尔;-
	OffShoreOnshoreSign string  `valid:"MaxSize(1)" description:"Offshore sign"`                                               //离在岸标志		N-不区分;A-在岸;L-离岸;-
	TermFlag            string  `valid:"MaxSize(1)" description:"Product staging mark"`                                        //产品分期标志			1-不分期；2-分期
	PrductBuyTime       int     `valid:"MaxSize(6)" description:"The number of times the product can be purchased repeatedly"` //产品可以重复购买次数		000000-不限次数；000001-一次
	BlncRflctsDrcton    string  `valid:"MaxSize(1)" description:"Balance reflects direction"`                                  //余额反映方向		D-借方;C-贷方;A-根据余额反映;O-空-
	AmtOpenMin          float64 `valid:"MaxSize(4)" description:"Minimum account opening amount"`                              //最低开户金额	0-不设置最低金额
	DepositNature       string  `valid:"MaxSize(3)" description:"Account nature"`                                              //账户性质	001-个人活期储蓄账户（1类）；002-个人活期储蓄账户（2类）；003-个人活期储蓄账户（3类）
	ApprvlMark          string  `valid:"MaxSize(1)" description:"Approved logo"`                                               //核准标识		Y-核准类;N-非核准类;-
	WdrwlMthd           string  `valid:"MaxSize(1)" description:"Withdrawal method"`                                           //取款方式		1-存折;2-密码;3-证件;4-印鉴;-
	TrnWdrwmThd         string  `valid:"MaxSize(1)" description:"Currency exchange method"`                                    //通兑方式		0-非通存通兑;1-分行通兑;2-全行通兑;-
	AppntDtFlg          string  `valid:"MaxSize(1)" description:"Whether to specify an expiry date"`                           //是否指定到期日 0-是;1-否;
	DepcreFlag          string  `valid:"MaxSize(1)" description:"Debit and Credit Control Flag"`                               //借贷记控制标志 N-正常;D-只借记;C-只贷记;F-不允许借贷记;-
	SttmntFlg           string  `valid:"MaxSize(1)" description:"Statement ID"`                                                //对账单标识		Y-出对账单;N-不出对账单;-
	SttmntType          string  `valid:"MaxSize(1)" description:"Reconciliation method"`                                       //对账方式		0-综合对账单;1-账户对账单;2-合约对账单-
	SttmntPeriod        string  `valid:"MaxSize(2)" description:"Statement cycle"`                                             //对账单周期		I-分钟;O-小时;D-日;W-周;M-月;S-季;H-半年;Y-年;-
	SttmntFrequency     int     `valid:"MaxSize(2)" description:"Statement frequency"`                                         //对账单频率
	SttmntDate          int     `valid:"MaxSize(2)" description:"The specified day of the statement"`                          //对账单指定日	99-每月最后一天
	SlpTrnChange        string  `description:"Real estate conversion sign"`                                                    //不动户转换标志	1-转久悬；2-转不动户
	StpTrnAut           string  `valid:"MaxSize(4)" description:"Whether to allow automatic transfer for a long time"`         //是否允许自动转久悬		0-是;1-否;2-未知-
	StpAmtMin           float64 `valid:"MaxSize(4)" description:"Minimum amount of fixed account"`                             //不动户最低限额
	StpGraceDate        int     `valid:"MaxSize(3)" description:"Long-term grace period for suspended households"`             //待转久悬户宽限天数
	StpNotDate          int     `valid:"MaxSize(3)" description:"Number of days of long-term notification"`                    //久悬户通知天数
	StpIntFlag          string  `valid:"MaxSize(1)" description:"Long-hanging account period interest-bearing indicator"`      //久悬户期间计息标识		0-是;1-否;
	StpUnclaimedDay     int     `valid:"MaxSize(6)" description:"Days of unclaimed funds"`                                     //无人认领资金天数
	CancelAutoFlag      string  `valid:"MaxSize(1)" description:"Whether to allow automatic account cancellation"`             //是否允许自动销户		0-是;1-否;2-未知-
	CancelAutoDay       int     `valid:"MaxSize(1)" description:"Automatic account cancellation grace days"`                   //自动销户宽限天数		0-不宽限
	CancelRepnFlag      string  `valid:"MaxSize(1)" description:"Cancel the account and enable the logo again"`                //销户再次启用标识		Y-启用;N-不启用;-
	CashAccessFlag      string  `valid:"MaxSize(1)" description:"Cash deposit and withdrawal logo"`                            //现金存取标识		A-允许现金存取;B-允许现金存，不允许现金取;C-允许现金取，不允许现金存;D-不允许现金存取;-
	//如果产品分期标志=2，则返回以下字段信息
	AmtDepositMin           float64 `valid:"MaxSize(4)" description:"Minimum deposit"`                              //最低起存金额
	ProductNum              int     `valid:"MaxSize(6)" description:"Product period"`                               //产品期数
	DepositDayMin           int     `valid:"MaxSize(6)" description:"Minimum number of days"`                       //最小存期天数
	DepositDayMax           int     `valid:"MaxSize(6)" description:"Maximum number of days"`                       //最大存期天数
	DepositPeriodUnit       string  `valid:"MaxSize(1)" description:"Deposit period unit"`                          //存期单位
	DepositPeriod           int     `valid:"MaxSize(6)" description:"Deposit period"`                               //存期
	DepositAgainFlag        string  `valid:"MaxSize(1)" description:"Whether to allow another deposit"`             //是否允许再次存款
	AdvanceWithdrawNum      int     `valid:"MaxSize(3)" description:"Allow early withdrawals"`                      //允许提前支取次数
	MaturityWithdrawNum     int     `valid:"MaxSize(6)" description:"Allowed number of withdrawals due"`            //允许到期支取次数
	OverdueWithdrawNum      int     `valid:"MaxSize(6)" description:"Number of allowed overdue withdrawals"`        //允许逾期支取次数
	AdvanceWithdrawGraceNum int     `valid:"MaxSize(3)" description:"Early withdrawal grace days"`                  //提前支取宽限天数
	WithdrawAmtMin          float64 `valid:"MaxSize(4)" description:"Minimum withdrawal amount"`                    //最低支取金额
	KeepAmtMin              float64 `valid:"MaxSize(4)" description:"Minimum retention amount"`                     //最低留存金额
	MaturityFlag            string  `valid:"MaxSize(1)" description:"Expiration processing indicator"`              //到期处理标识
	ConDepositNum           int     `valid:"MaxSize(6)" description:"Number of renewal periods"`                    //续存期数
	AmtConMin               float64 `valid:"MaxSize(4)" description:"Minimum renewal amount"`                       //最低续存金额
	BalprocessWay           string  `valid:"MaxSize(1)" description:"Principal processing method"`                  //本金处理方式
	BalConMatWay            string  `valid:"MaxSize(1)" description:"Principal renewal maturity processing method"` //本金续存到期处理方式
	PrinConMatWay           string  `valid:"MaxSize(1)" description:"How to deal with interest renewal maturity"`   //利息续存到期处理方式
	HolidayFlag             string  `valid:"MaxSize(1)" description:"Holiday processing sign"`                      //节假日处理标志

}

// @Desc Build request message
func (o *SSV9000002I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV9000002I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV9000002O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV9000002O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV9000002I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV9000002I) GetServiceKey() string {
	return "ssv9000002"
}
