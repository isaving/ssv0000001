//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package constant

const (
	SOLAPP_SDK_VERSION = "v0.2.1-beta.1"
)

const (
	COMM_DIRECT                    = "direct"
	COMM_MESH                      = "mesh"
	PARTICIPANT_ADDRESS_SPLIT_CHAR = "|"
	TOPIC_ID_SPLIT_CHAT            = "/"
)

/*dts agent service url path*/
const (
	ROOT_TXN_REGISTER_PATH       = "/v1/txn_mgt/root_txn/register"
	BRANCH_TXN_ENLIST_PATH       = "/v1/txn_mgt/branch_txn/enlist"
	TXN_TRY_RESULT_REPORT_PATH   = "/v1/txn_mgt/txn_try_result/report"
	ABNORMAL_TXN_PROCESSING_PATH = "/v1/txn_mgt/abnormal_txn/processing"
)

const (
	TXN_PROPAGATE_ROOT_XID_KEY      = "ROOT_XID"
	TXN_PROPAGATE_PARENT_XID_KEY    = "PARENT_XID"
	TXN_PROPAGATE_DTS_AGENT_ADDRESS = "DTS_AGENT_ADDRESS"
)

/* dts client service url path */
const (
	TXN_CALLBACK_CONFIRM = "/dts_supports/v1/callback/confirm"
	TXN_CALLBACK_CANCEL  = "/dts_supports/v1/callback/cancel"
)

/*topic*/
const (
	TOPIC_SPLIT_CHAR = "/"
)

const (
	MESSAGE_TYPE_JSON = "json"
	MESSAGE_OK        = "ok"
)

const (
	TOPIC_KEY = "TOPIC"
)

const (
	BASE_CONTROLLER_FIELD_NAME = "BaseHandler"
	MESH_REQ_FIELD_NAME        = "Req"
	MESH_RSP_FIELD_NAME        = "Rsp"
	DTS_CTX_FIELD_NAME         = "DTSCtx"
	CONTROLLER_NAME_GET_METHOD = "ControllerName"
	EVENT_HANDLE_FLAG          = "EVENT_HANDLE_FLAG"
	EVENT_HANDLE_FLAG_TRUE     = "1"
)

const (
	ENV_NAME_TOPIC_SOURCE_NODE_ID_KEY     = "NODE_ID"
	ENV_NAME_TOPIC_SOURCE_INSTANCE_ID_KEY = "INSTANCE_ID"

	DTS_AGENT_DEFAULT_NODE_ID = "NODE0001"
)

const (
	DEFAULT_ALERT_LEVEL = 0
	ALERT_LEVEL_YELLOW  = DEFAULT_ALERT_LEVEL // default level is yellow
	ALERT_LEVEL_ORANGE  = 1                   // orange
	ALERT_LEVEL_RED     = 2                   // red

)
