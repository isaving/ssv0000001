//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package config

import (
	"bufio"
	"crypto/md5"
	"fmt"
	log "git.forms.io/universe/comm-agent/common/log"
	"git.forms.io/universe/common/util"
	"github.com/astaxie/beego"
	"github.com/go-errors/errors"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"time"
)

var serviceRelationFilePath = func() string {
	filePath := beego.AppConfig.DefaultString("app::serviceRelationPath", "./conf/service_relation.conf")
	fmt.Fprintf(os.Stdout, "service relation file path:%s", filePath)
	return filePath
}()

// ServiceRelationMap, This global variable is used to store the upstream and downstream service list of service.
// the key is service id
//var ServiceRelationMap map[string]ServiceRelation

var ServiceRelationChangeChan chan map[string]ServiceRelation

// ServiceRelation, global variable to storing the upstream and downstream service lists of current service
type ServiceRelation struct {
	// UpstreamService storing the upstream service lists of current service
	UpstreamService []string

	// DownstreamService storing the downstream service list of current service
	DownstreamService []string
}

func init() {
	ServiceRelationChangeChan = make(chan map[string]ServiceRelation, 1024)
}

// LoadServiceRelationConfig, LoadService Relation Config is used to load all upstream and downstream
// service lists of current service. This method will be called when the comm-agent server starts.
// The read configuration file is fixed to "./service_realtion.conf"
func LoadServiceRelationConfig() error {
	log.Info("start load service relation config...")
	cryptoIsEnable, err := beego.AppConfig.Bool("crypto::enable")
	if nil != err {
		return errors.Wrap(err, 0)
	}

	if !cryptoIsEnable {
		log.Info("crypto is disabled, skip load service relation config...")
		return nil
	}

	if nil != err {
		if os.IsNotExist(err) {
			log.Infof("WARN:cannot found service relation file %s skip load", serviceRelationFilePath)
			return nil
		} else {
			return errors.Wrap(err, 0)
		}
	}

	if serviceRelationMap, err := getServiceRelationMap(serviceRelationFilePath); nil != err {
		return err
	} else {
		serviceId := beego.AppConfig.String("app::serviceId")
		if _, ok := serviceRelationMap[serviceId]; ok {
			if nil != serviceRelationMap[serviceId].UpstreamService {
				ServiceRelationChangeChan <- serviceRelationMap
			}
		}
	}

	log.Info("loaded service relation config successfully.")

	return nil
}

func getServiceRelationMap(filePath string) (map[string]ServiceRelation, error) {
	serviceRelationMap := make(map[string]ServiceRelation, 0)
	f, err := os.Open(filePath)
	defer f.Close()

	if nil != err {
		return nil, errors.Wrap(err, 0)
	}

	//ServiceRelationMap = make(map[string]ServiceRelation, 0)
	r := bufio.NewReader(f)
	lineNum := 0
	for {
		lineNum++

		b, eof, err := readLine(r)
		if nil != err {
			return nil, err
		}

		if eof {
			break
		}

		// Remove spaces at both ends of a single-line attribute
		s := strings.TrimSpace(string(b))

		// Judge the equal sign = position on the line
		index := strings.Index(s, "=")

		// Get the key value on the left side of the equal sign to determine if it is empty.
		if index < 0 || len(strings.TrimSpace(s[:index])) == 0 {
			return nil, errors.Errorf("invalid config format(cannot found service key) at line: %d", lineNum)
		}

		key := strings.TrimSpace(s[:index])
		udServices := strings.Split(s[index+1:], "|")

		upstreamServices, downstreamServices := getUpStreamAndDownStream(udServices)
		serviceRelationMap[key] = ServiceRelation{
			UpstreamService:   upstreamServices,
			DownstreamService: downstreamServices,
		}
	}

	return serviceRelationMap, nil
}

func StartServiceRelationFileWatch() error {
	cryptoIsEnable, err := beego.AppConfig.Bool("crypto::enable")
	if nil != err {
		return errors.Wrap(err, 0)
	}

	if !cryptoIsEnable {
		log.Info("crypto is disabled, skip load service relation config...")
		return nil
	}

	go startServiceRelationFileWatch(serviceRelationFilePath)

	return nil
}

func startServiceRelationFileWatch(filePath string) {
	defer func() {
		if e := recover(); e != nil {
			log.Errorf("comm-agent serviceRelationFileWatch panic:%v, please check", e)
		}
	}()

	md5Value := ""
	var upstreamServices []string
	f, err := os.Open(filePath)
	if err != nil {
		log.Errorf("Open file failed: %v", err)
	} else {
		body, err := ioutil.ReadAll(f)
		f.Close()
		if err != nil {
			log.Errorf("Read file failed: %v", err)
		} else {
			b := md5.Sum(body)
			md5Value = string(b[:])
		}

	}
	serviceId := beego.AppConfig.String("app::serviceId")
	serviceRelationMap, err := getServiceRelationMap(filePath)
	if nil == err {
		if r, ok := serviceRelationMap[serviceId]; ok {
			upstreamServices = r.UpstreamService
		}
	}

	timer := time.NewTicker(time.Second * 1)
	for {
		select {
		case <-timer.C:
			{
				f, err = os.Open(filePath)
				if err != nil {
					log.Debugf("Open file failed: %v, skip check", err)
					continue
				}

				body, err := ioutil.ReadAll(f)
				f.Close()
				if err != nil {
					log.Debugf("Read file failed: %v, skip check", err)
					continue
				}

				b := md5.Sum(body)
				currentMd5Value := string(b[:])
				if "" == md5Value || md5Value != currentMd5Value {
					serviceRelationMap, err := getServiceRelationMap(filePath)
					if nil != err {
						log.Errorf("get service relation map failed, error=%++v", err)
						continue
					}

					var pickedUpstreamService []string
					if r, ok := serviceRelationMap[serviceId]; ok {
						pickedUpstreamService = r.UpstreamService
					}

					if !util.IsArrayEqual(upstreamServices, pickedUpstreamService) {
						ServiceRelationChangeChan <- serviceRelationMap
						upstreamServices = pickedUpstreamService
					}

					md5Value = currentMd5Value
				}
			}
		}
	}
}

func getUpStreamAndDownStream(udServices []string) ([]string, []string) {
	var upstreamServices []string
	var downstreamServices []string
	if len(udServices) == 2 {
		// read upstream
		if len(strings.TrimSpace(udServices[0])) > 0 {
			upstreamServices = strings.Split(strings.TrimSpace(udServices[0]), ",")
		}
		// read downstream
		if len(strings.TrimSpace(udServices[1])) > 0 {
			downstreamServices = strings.Split(strings.TrimSpace(udServices[1]), ",")
		}
	} else if len(udServices) == 1 {
		// read upstream
		upstreamServices = strings.Split(strings.TrimSpace(udServices[0]), ",")
	}

	return upstreamServices, downstreamServices
}

func readLine(r *bufio.Reader) ([]byte, bool, error) {
	b, _, err := r.ReadLine()
	if err != nil {
		if err == io.EOF {
			return nil, true, nil
		}
		return nil, false, err
	}

	return b, false, nil
}
