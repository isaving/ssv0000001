//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package crypto

import "time"

const (
	DateFormat = "2006-01-02"
	TimeFormat = "2006-01-02 15:04:05"
)

type Date time.Time

func Now() Date {
	return Date(time.Now())
}

func (t *Date) UnmarshalJSON(data []byte) (err error) {
	now, err := time.ParseInLocation(`"`+TimeFormat+`"`, string(data), time.Local)
	*t = Date(now)
	return
}

func (t Date) MarshalJSON() ([]byte, error) {
	b := make([]byte, 0, len(TimeFormat)+2)
	b = append(b, '"')
	b = time.Time(t).AppendFormat(b, TimeFormat)
	b = append(b, '"')
	return b, nil
}

func (t Date) String() string {
	return time.Time(t).Format(TimeFormat)
}

// KmsConfig is used to store Kms related configuration information.
type KmsConfig struct {
	KeySpanDuration   uint
	KeyEffectDuration uint
	KeyVersion        string
}

// KmsServiceKey is used to store information related to the service key value.
type KmsServiceKey struct {
	ServiceId string
	Version   string
	KeyId     string
	KeyValue  string
	KeyState  int
}

// KeyType is alias of string
type KeyType string

// KmsKey is used to store the details of each key
type KmsKey struct {
	ServiceId   string
	SystemType  string
	KeyType     KeyType
	KeyId       string
	KeyVersion  int
	KeyValue    string
	State       int
	EffectTime  Date
	DeffectTime Date
	Creator     string
	CreateTime  Date
	Modifier    string
	LastUpdate  Date
}

// A type, typically a collection, that satisfies sort.Interface can be
// sorted by the routines in this package. The methods require that the
// elements of the collection be enumerated by an integer index.
type KmsKeySlice []KmsKey

// Len is the number of elements in the collection.
func (s KmsKeySlice) Len() int { return len(s) }

// Swap swaps the elements with indexes i and j.
func (s KmsKeySlice) Swap(i, j int) { s[i], s[j] = s[j], s[i] }

// Less reports whether the element with
// index i should sort before the element with index j.
func (s KmsKeySlice) Less(i, j int) bool {
	return time.Time(s[i].EffectTime).After(time.Time(s[j].EffectTime))
}

// KmsKeyResult is used to store key information
type KmsKeyResult struct {
	Code      int
	Error     string
	ServiceId string
	Result    KmsKeySlice
}

// KmsOperKeyResult is used to store the results of kms operations
type KmsOperKeyResult struct {
	Code   int
	Error  string
	Result bool
}

// KmsServiceRequest is used to store the request structure that the client requests
// to obtain a single service key information from the Kms adapter.
type KmsServiceRequest struct {
	KeyType     KeyType
	ServiceId   string
	SystemType  string
	EffectTime  Date
	DeffectTime Date
	Operator    string
	RequestTime Date
}

// KmsServicesRequest is used to store the request structure that
// the client requests to obtain multiple service key information from the Kms adapter.
type KmsServicesRequest struct {
	KeyType    KeyType
	ServiceIds []string
}

// KmsKeyRequest is a structure for obtaining a single key information based on the key id
type KmsKeyRequest struct {
	KeyId string
}

// KmsKeyRequest is a structure for obtaining multiple key information according to the key id array
type KmsKeysRequest struct {
	KeyId []string
}

// KmsKeyIdResponse stores a single key id
type KmsKeyIdResponse struct {
	KeyId string
}

// A request structure for storing a client requesting a Kms adapter
// to obtain a plurality of different key types and different service key information.
type KmsKeyValuesRequest struct {
	Services []KmsServiceRequest
}

//KmsKeysResultResponse stores the execution result of multiple kms operations
type KmsKeysResultResponse struct {
	Total  int
	Succ   int
	Result []KmsOperKeyResult
}

// KmsKeysRootResponse stores a single service key information
type KmsKeysRootResponse struct {
	Code    int
	Message string
	Data    KmsKeysResponse
}

// KmsKeysResponse is used to store multiple service key information
type KmsKeysResponse struct {
	Total  int
	Succ   int
	Result []KmsKeyResult
}

// KmsKeyValuesResponse is used to store information related to multiple service key values.
type KmsKeyValuesResponse struct {
	Total     int
	Succ      int
	KeyValues []KmsServiceKey
}

// KmsDestroyKeysRequest is used to store the request message structure for destroying multiple service keys.
type KmsDestroyKeysRequest struct {
	Services []KmsServiceRequest
}

// KmsServiceKeyUpdateNotice is used by KMS Adapter
// to notify related services for key update
//type KmsServiceKeyUpdateNotice struct {
//	ServiceId string
//	Key string
//	RequestTime  time.Time
//}

// KmsRequestCrypto for all request that need to be encrypted.
type KmsRequestCrypto struct {
	Request         string
	Key             string
	CryptoAlgorithm string
}

// KmsResponseCrypto for all response that need to be decrypted
type KmsResponseCrypto struct {
	Response string
}
