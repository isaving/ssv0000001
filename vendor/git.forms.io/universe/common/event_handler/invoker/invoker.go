//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package invoker

import (
	"git.forms.io/universe/comm-agent/client"
	log "git.forms.io/universe/comm-agent/common/log"
	common "git.forms.io/universe/comm-agent/common/protocol"
	"git.forms.io/universe/common/event_handler/base"
	"git.forms.io/universe/common/event_handler/register"
	"github.com/go-errors/errors"

	//"github.com/pkg/errors"
	"reflect"
	"strings"
)

func InvokeService(msg client.UserMessage) (r *client.UserMessage, err error) {
	r = &client.UserMessage{}
	if (strings.EqualFold(common.TOPIC_TYPE_HEARTBEAT, msg.GetMsgTopicType())) ||
		strings.EqualFold(common.TOPIC_TYPE_ERROR, msg.GetMsgTopicType()) ||
		strings.EqualFold(common.TOPIC_TYPE_ALERT, msg.GetMsgTopicType()) ||
		strings.EqualFold(common.TOPIC_TYPE_TRANSACTION, msg.GetMsgTopicType()) ||
		strings.EqualFold(common.TOPIC_TYPE_LOG, msg.GetMsgTopicType()) ||
		strings.EqualFold(common.TOPIC_TYPE_METRICS, msg.GetMsgTopicType()) ||
		strings.EqualFold(common.TOPIC_TYPE_DTS, msg.GetMsgTopicType()) ||
		strings.EqualFold(common.TOPIC_TYPE_UNIVERSE, msg.GetMsgTopicType()) {
		topicId := msg.GetMsgTopicId()
		handlerProperty, error := register.Router.MatchHandler(topicId)
		if nil != error {
			return nil, error
		}
		if nil != handlerProperty {
			log.Debugf("start invoke service topic id[%s], method:[%s]", topicId, handlerProperty.MethodName)
			handler := reflect.New(handlerProperty.HandlerType).Interface().(base.EventHandlerInterface)

			handler.SetEventCallback()
			handlerInstance := reflect.ValueOf(handler)
			targetRunMethod := handlerInstance.MethodByName(handlerProperty.MethodName)
			handler.SetRequest(msg)

			handler.EventHandlePrepare()

			targetRunMethod.Call(nil)

			handler.EventHandleFinish()

			res := handler.GetResponse()

			return res, nil
		} else {
			err = errors.Errorf("Cannot found handler with topic id: %s", topicId)
			log.Errorf("Invoke service failed, error = %v", err)
			return r, err
		}
	} else {
		err = errors.Errorf("Invalid topic type: %s", msg.GetMsgTopicType())
		return r, err
	}
}
