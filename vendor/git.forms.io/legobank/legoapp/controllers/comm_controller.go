package controllers

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/context"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/remote"
	"git.forms.io/universe/comm-agent/client"
	"git.forms.io/universe/common/event_handler/base"
	"git.forms.io/universe/dts/client/controllers"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/beego/i18n"
	"strings"
)

// CommTCCController defines some have TCC event message request handler operations, such as
// context prepare finish
type CommTCCController struct {
	controllers.DTSBaseController
	ServiceError        *errors.ServiceError
	Lang                string
	Ctx                 *context.ServiceContext
	SrcAppProps         map[string]string
	RemoteCallInterface remote.RemoteCallInterface
}

// CommController defines some event message request handler operations, such as
// context prepare finish
type CommController struct {
	base.BaseController
	ServiceError        *errors.ServiceError
	Lang                string
	Ctx                 *context.ServiceContext
	SrcAppProps         map[string]string
	RemoteCallInterface remote.RemoteCallInterface
}

//EventHandlePrepare runs after Init before request function execution.
func (c *CommController) EventHandlePrepare() {
	if nil == c.Req {
		c.Req = &client.UserMessage{}
	}

	if nil == c.Req.AppProps {
		c.Req.AppProps = make(map[string]string)
	}
	log.Infof_V4("Request:%++v", c.Req)
	c.Lang = c.Req.AppProps[constant.USERLANG]
	if "" == c.Lang {
		c.Lang = constant.ENUS
	}

	c.Ctx = &context.ServiceContext{
		TraceId:      c.Req.AppProps[constant.GLOBALBIZSEQNO],
		SpanId:       c.Req.AppProps[constant.SRCBIZSEQNO],
		ParentSpanId: c.Req.AppProps[constant.PARENT_SPAN_ID],
		Topic:        c.Req.GetMsgTopicId(),
	}
	c.backupAppProps()

	if c.Ctx.SpanId == "" {
		c.Req.AppProps[constant.PARENT_SPAN_ID] = c.Ctx.TraceId
	} else {
		c.Req.AppProps[constant.PARENT_SPAN_ID] = c.Ctx.SpanId
	}
}

func (c *CommController) backupAppProps() {
	c.SrcAppProps = make(map[string]string)
	c.SrcAppProps[constant.PARENT_SPAN_ID] = c.Req.AppProps[constant.PARENT_SPAN_ID]
}

// EventHandleFinish runs after request function execution.
func (c *CommController) EventHandleFinish() {

	c.restoreAppProps()
	if c.ServiceError != nil {
		errorCode := c.ServiceError.ErrorCode
		errorArgs := c.ServiceError.ErrorArgs
		if nil != c.ServiceError.Err {
			log.Errorf("Service invoke failed, error=%++v", c.ServiceError.Err)
		}
		errorMsg := i18n.Tr(c.Lang, "errors."+errorCode, errorArgs)
		if errorMsg == "" || strings.Contains(errorMsg, "!(EXTRA") || errorMsg == errorCode {
			errorMsg = c.ServiceError.Error()
		}
		rspBody := []byte(`{"errorCode":"` + errorCode + `","errorMsg":"` + errorMsg + `","response":{}}`)
		c.Req.AppProps[constant.RETMSGCODE] = errorCode
		c.Req.AppProps[constant.RETMESSAGE] = errorMsg
		c.Req.AppProps[constant.RETSTATUS] = "F"
		c.Rsp = &client.UserMessage{
			AppProps: c.Req.AppProps,
			Body:     rspBody,
		}
	}
	log.Infof_V4("Response:%++v", c.Rsp)
}

func (c *CommController) restoreAppProps() {

	if c.Req.AppProps == nil {
		c.Req.AppProps = make(map[string]string)
	}
	for key, value := range c.SrcAppProps {
		c.Req.AppProps[key] = value
	}

}

// The Prepare() of EventHandlePrepare, which finds out DTS information from Request
func (c *CommTCCController) EventHandlePrepare() {
	if nil == c.Req {
		c.Req = &client.UserMessage{}
	}

	if nil == c.Req.AppProps {
		c.Req.AppProps = make(map[string]string)
	}
	log.Infof_V4("Request:%++v", c.Req)
	c.DTSBaseController.EventHandlePrepare()

	c.Ctx = &context.ServiceContext{
		TraceId:      c.Req.AppProps[constant.GLOBALBIZSEQNO],
		SpanId:       c.Req.AppProps[constant.SRCBIZSEQNO],
		ParentSpanId: c.Req.AppProps[constant.PARENT_SPAN_ID],
		Topic:        c.Req.GetMsgTopicId(),
	}
	c.backupAppProps()

	if c.Ctx.SpanId == "" {
		c.Req.AppProps[constant.PARENT_SPAN_ID] = c.Ctx.TraceId
	} else {
		c.Req.AppProps[constant.PARENT_SPAN_ID] = c.Ctx.SpanId
	}
	c.Lang = c.Req.AppProps[constant.USERLANG]
	if "" == c.Lang {
		c.Lang = constant.ENUS
	}
}

func (c *CommTCCController) backupAppProps() {
	c.SrcAppProps = make(map[string]string)
	c.SrcAppProps[constant.PARENT_SPAN_ID] = c.Req.AppProps[constant.PARENT_SPAN_ID]
}

// The Finish() of DTSClientBaseHandler, which describes response according to transaction's result
func (c *CommTCCController) EventHandleFinish() {

	c.restoreAppProps()
	if c.ServiceError != nil {
		errorCode := c.ServiceError.ErrorCode
		errorArgs := c.ServiceError.ErrorArgs
		if nil != c.ServiceError.Err {
			log.Errorf("Service invoke failed, error=%++v", c.ServiceError.Err)
		}
		errorMsg := i18n.Tr(c.Lang, "errors."+errorCode, errorArgs)
		if errorMsg == "" || strings.Contains(errorMsg, "!(EXTRA") || errorMsg == errorCode {
			errorMsg = c.ServiceError.Error()
		}
		rspBody := []byte(`{"errorCode":"` + errorCode + `","errorMsg":"` + errorMsg + `","response":{}}`)
		c.Req.AppProps[constant.RETMSGCODE] = errorCode
		c.Req.AppProps[constant.RETMESSAGE] = errorMsg
		c.Req.AppProps[constant.RETSTATUS] = "F"
		c.Rsp = &client.UserMessage{
			AppProps: c.Req.AppProps,
			Body:     rspBody,
		}
	}
	log.Infof_V4("dts response: %++v", c.Rsp)
	c.DTSCtx = nil
}

func (c *CommTCCController) restoreAppProps() {

	if c.Req.AppProps == nil {
		c.Req.AppProps = make(map[string]string)
	}
	for key, value := range c.SrcAppProps {
		c.Req.AppProps[key] = value
	}

}

//Set response message
func (c *CommController) SetAppResponse(appProps map[string]string, body []byte) {
	appProps[constant.RETMSGCODE] = ""
	appProps[constant.RETMESSAGE] = ""
	appProps[constant.RETSTATUS] = "N"
	c.SetResponse(&client.UserMessage{
		AppProps: appProps,
		Body:     body,
	})
}

//Set response message
func (c *CommController) SetAppBody(body []byte) {
	c.Req.AppProps[constant.RETMSGCODE] = ""
	c.Req.AppProps[constant.RETMESSAGE] = ""
	c.Req.AppProps[constant.RETSTATUS] = "N"
	c.SetResponse(&client.UserMessage{
		AppProps: c.Req.AppProps,
		Body:     body,
	})
}

//Set response message
func (c *CommTCCController) SetAppResponse(appProps map[string]string, body []byte) {
	appProps[constant.RETMSGCODE] = ""
	appProps[constant.RETMESSAGE] = ""
	appProps[constant.RETSTATUS] = "N"
	c.SetResponse(&client.UserMessage{
		AppProps: appProps,
		Body:     body,
	})
}

//Set response message
func (c *CommTCCController) SetAppBody(body []byte) {
	c.Req.AppProps[constant.RETMSGCODE] = ""
	c.Req.AppProps[constant.RETMESSAGE] = ""
	c.Req.AppProps[constant.RETSTATUS] = "N"
	c.SetResponse(&client.UserMessage{
		AppProps: c.Req.AppProps,
		Body:     body,
	})
}

//Set error response message
func (c *CommController) SetServiceError(e interface{}) {
	switch e := e.(type) {
	case *errors.ServiceError:
		c.ServiceError = e
	case errors.ServiceError:
		c.ServiceError = &e
	default:
		c.ServiceError = errors.Errorf(constant.INVALIDERR, "Invalid service error:[%++v]", e)
	}
	c.Err = c.ServiceError.Err
}

//Set error response message
func (c *CommTCCController) SetServiceError(e interface{}) {
	switch e := e.(type) {
	case *errors.ServiceError:
		c.ServiceError = e
	case errors.ServiceError:
		c.ServiceError = &e
	default:
		c.ServiceError = errors.Errorf(constant.INVALIDERR, "Invalid service error:[%++v]", e)
	}
	c.Err = c.ServiceError.Err
}
