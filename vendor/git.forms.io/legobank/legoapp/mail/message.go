package mail

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"git.forms.io/legobank/legoapp/mysmtp"
	"git.forms.io/universe/solapp-sdk/log"
	"io/ioutil"
	"net/smtp"
	"strings"
	"time"
)

const boundary = "544849535f49535f424f554e444152595f4c494e45"

type Attachment struct {
	FileName    string
	ContentType string
	Data        []byte
	Inline      bool
}
type Message struct {
	DumpForm    string
	From        string
	To          []string
	Cc          []string
	Bcc         []string
	Subject     string
	Body        string
	ContentType string
	Attachment  *Attachment
	Attachments map[string]*Attachment
}

func SendMsg(addr string, auth smtp.Auth, m *Message) error {
	if m == nil {
		return fmt.Errorf("can not found message")
	}
	return mysmtp.NewSendMail(addr, auth, m.From, m.To, m.Bytes())
}

func (m *Message) Bytes() []byte {
	buffer := bytes.NewBuffer(nil)
	header := make(map[string]string)
	header["From"] = m.From
	if m.DumpForm != "" {
		header["Sender"] = m.DumpForm
		header["From"] = m.DumpForm
	}
	header["To"] = strings.Join(m.To, ";")
	header["Cc"] = strings.Join(m.Cc, ";")
	header["Bcc"] = strings.Join(m.Bcc, ";")
	header["Subject"] = m.Subject
	header["Content-Type"] = "multipart/mixed;boundary=" + boundary
	header["Mime-Version"] = "1.0"
	header["Date"] = time.Now().Format(time.RFC822)
	buildHeader(buffer, header)

	body := "\r\n--" + boundary + "\r\n"
	body += "Content-Type:" + m.ContentType + "\r\n"
	body += "\r\n" + m.Body + "\r\n"
	buffer.WriteString(body)

	if m.Attachment != nil {
		attachment := "\r\n--" + boundary + "\r\n"
		attachment += "Content-Type:" + m.Attachment.ContentType + "\r\n"
		attachment += "Content-Transfer-Encoding:base64\r\n"
		attachment += "Content-Disposition:attachment; filename=\"" + m.Attachment.FileName + "\"\r\n"
		buffer.WriteString(attachment)
		if m.Attachment.Data != nil {
			buffer.WriteString("\r\n")
			buffer.Write(m.Attachment.Data)
			buffer.WriteString("\r\n")
		} else {
			if err := writeFile(buffer, m.Attachment.FileName); err != nil {
				log.Errorf("write attachment [%s] failed, %v", m.Attachment.FileName, err)
			}
		}
	}
	buffer.WriteString("\r\n--" + boundary + "--")
	log.Debugf("mail bytes success: %s", buffer.String())
	return buffer.Bytes()
}

func writeFile(buffer *bytes.Buffer, fileName string) error {
	file, err := ioutil.ReadFile(fileName)
	if err != nil {
		return err
	}
	payload := make([]byte, base64.StdEncoding.EncodedLen(len(file)))
	base64.StdEncoding.Encode(payload, file)
	buffer.WriteString("\r\n")
	for index, line := 0, len(payload); index < line; index++ {
		buffer.WriteByte(payload[index])
		if (index+1)%76 == 0 {
			buffer.WriteString("\r\n")
		}
	}
	return nil
}

func buildHeader(buffer *bytes.Buffer, header map[string]string) {
	s := ""
	for key, value := range header {
		s += key + ":" + value + "\r\n"
	}
	s += "\r\n"
	buffer.WriteString(s)
}
