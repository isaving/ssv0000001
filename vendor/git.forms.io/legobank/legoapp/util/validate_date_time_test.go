package util

import "testing"

func TestValidateDate(t *testing.T) {

	date1 := "20191128"
	date2 := "2019-11-28"

	if err := ValidateDate(date1); err != nil {
		t.Log(err)
	} else {
		t.Errorf("Validate date %v failed.", date1)
	}

	if err := ValidateDate(date2); err != nil {
		t.Errorf("Validate date %v failed.", date2)
	}

}

func TestValidateTime(t *testing.T) {
	time1 := "15:49:00"
	time2 := "15:52"
	if err := ValidateTime(time1); err != nil {
		t.Error(err)
	}

	if err := ValidateTime(time2); err == nil {
		t.Errorf("Validate time: %v failed.", time2)
	} else {
		t.Log(err)
	}

}

func TestValidateDateTime(t *testing.T) {

	datetime1 := "201911281500"
	datetime2 := "2019-11-28 15:49:00"
	datetime3 := "2019-11-2815:49:00"

	if err := ValidateDateTime(datetime1); err == nil {
		t.Errorf("Validate time: %v failed.", datetime1)
	} else {
		t.Log(err)
	}

	if err := ValidateDateTime(datetime2); err != nil {
		t.Error(err)
	} else {
		t.Log(err)
	}

	if err := ValidateDateTime(datetime3); err == nil {
		t.Error(err)
	} else {
		t.Log(err)
	}

}
