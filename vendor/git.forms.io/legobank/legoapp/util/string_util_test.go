package util

import (
	"testing"
)

// @Truncation string from start based on the length
func TestSubstr(t *testing.T) {
	str := "abcdefg"
	start := 2
	len := 3
	rpstr := Substr(str, start, len)
	t.Logf("rpstr %v", rpstr)

}

// @Truncation string from start to end
func TestSubstr2(t *testing.T) {
	str := "abcdefg"
	start := 2
	end := 5
	rpstr := Substr2(str, start, end)
	t.Logf("rpstr %v", rpstr)
}

// @struct to map
func TestStructToMap(t *testing.T) {
	type People struct {
		Name string `json:"name_title"`
		Age  int    `json:"age_size"`
	}

	student := People{"Mary", 18}
	data := StructToMap(student)
	t.Logf("data %v", data)
}
