package util

import (
	"git.forms.io/universe/solapp-sdk/config"
	"strings"
	"testing"
)

func TestSerialNo(test *testing.T) {

	config.CmpSvrConfig.DcnNo = "000000"
	config.CmpSvrConfig.InstanceID = "ac1dpp0100"
	businessNo := getBusinessSerialNo()
	test.Logf("serviaNo: %s", businessNo)

	if len(businessNo) != 33 {
		test.Error("generate businessNo failed.")
	}

	if !strings.HasPrefix(businessNo, "1") {
		test.Error("generate businessNo prefix error.")
	}

	globalNo := GetGlobalSerialNo()
	test.Logf("serviaNo: %s", globalNo)

	if len(globalNo) != 33 {
		test.Error("generate globalNo failed.")
	}

	if !strings.HasPrefix(globalNo, "0") {
		test.Error("generate globalNo prefix error.")
	}
}
