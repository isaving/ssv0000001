package util

import "testing"

func TestGetCurrentDate(t *testing.T) {
	date := GetCurrentDate()
	t.Logf("time %v", date)
}

func TestGetTimeFormat(t *testing.T) {
	timeFmt := GetTimeFormat(0)
	t.Logf("timeFmt %v", timeFmt)
}

func TestGetCurTime(t *testing.T) {
	time := GetCurTime
	t.Logf("time %v", time())
}

func TestGetCurrentDateTime(t *testing.T) {
	dateTime := GetCurrentDateTime
	t.Logf("dateTime %v", dateTime())
}

func TestGetCurrentTimeFmt(t *testing.T) {
	timeFmt := GetCurrentTimeFmt
	t.Logf("timeFmt %v", timeFmt())
}

func TestGetFirstDateOfMonth(t *testing.T) {
	firstDateOfMonth := GetFirstDateOfMonth()
	t.Logf("firstDateOfMonth %v", firstDateOfMonth)
}

func TestGetPreMonthDate(t *testing.T) {
	getPreMonthDate := GetPreMonthDate(1)
	t.Logf("GetPreMonthDate %v", getPreMonthDate)
}

func TestGetTHCurrentDateTime(t *testing.T) {
	getTHCurrentDateTime := GetTHCurrentDateTime()
	t.Logf("getTHCurrentDateTime %v", getTHCurrentDateTime)
}

func TestValidDate(t *testing.T) {
	validDate := ValidDate("2019-01-23 01:01:01")
	t.Logf("validDate %v", validDate)
}

func TestGetCurrentTimeStamp(t *testing.T) {
	date, time := GetCurrentTimeStamp()
	t.Logf("date %v,time %v ", date, time)
}

func TestGetTimeParse(t *testing.T) {
	time, err := GetTimeParse("2019-01-23 01:01:01")
	if err != nil {
		t.Error(err)
	} else {
		t.Logf("deBaseStr:%v", time)
	}

	time, err = GetTimeParse("2019-01-23 01:01:0")
	if err == nil {
		t.Error(err)
	}
}
