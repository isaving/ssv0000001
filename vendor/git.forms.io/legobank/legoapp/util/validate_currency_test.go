package util

import "testing"

func TestValidateCurrency(t *testing.T) {

	cny := "CNY"

	if err := ValidateCurrency(cny); err != nil {
		t.Error(err)
	}

	notCcy := "ABC"

	if err := ValidateCurrency(notCcy); err == nil {
		t.Error("Not currency validate failed.")
	}

}
