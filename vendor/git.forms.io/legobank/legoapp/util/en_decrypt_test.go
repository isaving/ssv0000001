package util

import (
	"testing"
)

const (
	AESKEY    = `abcdefghijk01234`
	RSAPUBKEY = `-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDQW26DPZOJ6m9I8jOkcLyCdJ+7
5jH0nZ9aQ4YfudvKQu2PoRL/3GOFspTN91tNN+prP2OIDL/+PTCdSykYtCj/iz+F
SvOfqAhW4xpJC9nY9qb8OIEj/JF7D9P14EwVfbTLP39QGRN7tzvSCVsvi4IuHteB
XcLhCjXribR4OwBUtQIDAQAB
-----END PUBLIC KEY-----`
	RSAPRIKEY = `-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQDQW26DPZOJ6m9I8jOkcLyCdJ+75jH0nZ9aQ4YfudvKQu2PoRL/
3GOFspTN91tNN+prP2OIDL/+PTCdSykYtCj/iz+FSvOfqAhW4xpJC9nY9qb8OIEj
/JF7D9P14EwVfbTLP39QGRN7tzvSCVsvi4IuHteBXcLhCjXribR4OwBUtQIDAQAB
AoGBAJyTuU8DHTbNaGfnK4vt9JwMzGVGLlBni3Mnv0lkQJmVBjWjlEWW92tnso+9
zW0QEigacM+y1S1YCoqKCw43hX54BsZhWWNKCrbywi7HuRHTxDiYmzeFriwb3V6c
yT1S4IF1WJ0PV11GE6z8E9RXzNm8x4YyKV/vOLWprXkAsUOZAkEA/l9pLV8CjY/a
+kv+uNSL8PMDFeG4QtQjWr3VUSyOEqlqxASJyE9feTLNNHMKLUeUSem8tuLOOs8h
io6arTeSFwJBANGwqSAbogC681SNyTJjfYvSHpkQjHp+CE2Q/orx2+ptQL6LHW8G
xBEerbG2W427bgZyz3HJE8DKuHo9heeiixMCQELkMDJgM7mqRLlhVqCgNBSusvcE
HoJkas+n7/qirtG1f4AAeYMVbFPgyhmqzthNZG9zac7mIblRUs6u5Xn7jQ0CQQCK
3h4DP4mYRjcjxgp6rlzUnkDZSZZfANxq0h1g0y+7HsOD7Ql/H9H1LIYHbSdQw9Hn
v10Wk/3a7eYiPmBRuNm1AkAgc1LNlYXLBmbqvYIJqN3vL5bqWLslzVL6ZMIyDxQo
S0hewa6JO+o+76A+Lk+jIWyLYp4j0RUw69PDYbNgGCk1
-----END RSA PRIVATE KEY-----`
)

func TestAesEncrypt(t *testing.T) {
	str1 := "test"
	if enStr, err := AesEncrypt([]byte(str1), []byte(AESKEY)); nil != err {
		t.Error(err)
	} else {
		t.Logf("enStr:%v", enStr)
	}
	if _, err := AesEncrypt([]byte(str1), []byte("")); nil == err {
		t.Error(err)
	}
}
func TestAesDecrypt(t *testing.T) {
	enStr1 := []byte{83, 18, 96, 135, 178, 4, 102, 39, 238, 39, 210, 218, 185, 3, 172, 233}
	if decryptStr, err := AesDecrypt(enStr1, []byte(AESKEY)); nil != err {
		t.Error(err)
	} else {
		t.Logf("decryptStr:%v", string(decryptStr))
	}
	if _, err := AesDecrypt(enStr1, []byte("")); nil == err {
		t.Error(err)
	}
}
func TestRsaEncrypt(t *testing.T) {
	str1 := "test"
	if enStr, err := RsaEncrypt([]byte(str1), []byte(RSAPUBKEY)); nil != err {
		t.Error(err)
	} else {
		t.Logf("enStr:%v", enStr)
	}
	if _, err := RsaEncrypt([]byte(str1), []byte("")); nil == err {
		t.Error(err)
	}
}
func TestRsaDecrypt(t *testing.T) {
	enStr1 := []byte{195, 4, 52, 180, 91, 205, 40, 79, 181, 55, 134, 236, 66, 207, 113, 14, 144, 81, 171, 202, 109, 162, 135, 132, 170, 188, 55, 252, 196, 229, 51, 77, 4, 240, 128, 2, 21, 209, 20, 34, 148, 170, 69, 248, 133, 247, 199, 118, 23, 224, 4, 82, 78, 186, 92, 168, 182, 182, 12, 126, 73, 99, 17, 187, 88, 214, 185, 133, 200, 137, 64, 236, 17, 169, 215, 218, 103, 222, 173, 92, 43, 156, 114, 153, 62, 119, 10, 234, 164, 150, 47, 190, 240, 196, 98, 96, 236, 113, 41, 12, 71, 139, 161, 22, 72, 206, 146, 61, 94, 204, 119, 189, 194, 91, 159, 8, 140, 214, 37, 114, 163, 42, 179, 118, 151, 23, 17, 213}
	if decryptStr, err := RsaDecrypt(enStr1, []byte(RSAPRIKEY)); nil != err {
		t.Error(err)
	} else {
		t.Logf("decryptStr:%v", string(decryptStr))
	}
	if _, err := RsaDecrypt(enStr1, []byte("")); nil == err {
		t.Error(err)
	}
}
func TestBaseEncode(t *testing.T) {
	str1 := "test"
	baseEnStr := BaseEncode([]byte(str1))
	t.Logf("baseEnStr:%v", baseEnStr)
}
func TestBaseDecode(t *testing.T) {
	baseEnStr := "dGVzdA=="
	deBaseStr, err := BaseDecode(baseEnStr)
	if err != nil {
		t.Error(err)
	} else {
		t.Logf("deBaseStr:%v", string(deBaseStr))
	}
	baseEnStr2 := "dGVzdA="
	_, err = BaseDecode(baseEnStr2)
	if err == nil {
		t.Error(err)
	}
}
