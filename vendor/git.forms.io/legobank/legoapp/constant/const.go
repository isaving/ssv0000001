package constant

//Global system variables are defined in the header
const (
	PARENT_SPAN_ID = "ParentBizSeqNo"

	GLOBALBIZSEQNO    = "GlobalBizSeqNo"    //Global unique business serial number
	ORGSYSID          = "OrgSysId"          //Original system ID (unchanged throughout transaction flow)
	ORGCHANNELTYPE    = "OrgChannelType"    //Business origination channel type (unchanged throughout the transaction flow)
	ORGSCENARIOID     = "OrgScenarioId"     //Business initiation scenario number (unchanged throughout the entire transaction flow)
	ORGPARTNERID      = "OrgPartnerId"      //Identification of the partner who initiated the business request
	SRCSYSID          = "SrcSysId"          //Source system code
	SRCDCN            = "SrcDcn"            //Source system DCN
	SRCSYSVERSION     = "SrcSysVersion"     //Source system version
	SRCSERVERID       = "SrcServerId"       //Service consumer server identification (server name or ip address)
	SRCSERVICEID      = "SrcServiceId"      //Service consumer service identification
	SRCTIMESTAMP      = "SrcTimeStamp"      //Source system initiates service call timestamp
	SRCTOPOICID       = "SrcTopoicId"       //Source service ID (Topoic or URL)
	SRCBIZSEQNO       = "SrcBizSeqNo"       //Source service business flow
	SRCBIZDATE        = "SrcBizDate"        //Source Service Business Date
	TXWORKSTATION     = "TxWorkStation"     //Trading terminal (logical terminal)
	TXDEVICEID        = "TxDeviceId"        //Transaction Initiating Device Terminal Number (eg. IMEI Code)
	TXDEPTCODE        = "TxDeptCode"        //Trading department (branch line number)
	REVERSESEQNO      = "ReverseSeqNo"      //This field is required if it is a rectification service
	USERLANG          = "UserLang"          //ISO International Language Code
	TXUSERID          = "TxUserId"          //Teller user id
	TXUSERLVL         = "TxUserLvl"         //Teller user level
	TXAUTHORIZER      = "TxAuthorizer"      //Authorizer
	TXAUTHUSERID      = "TxAuthUserId"      //Authorized user id
	TXAUTHUSERLVL     = "TxAuthUserLvl"     //Authorized user level
	TXAUTHFLAG        = "TxAuthFlag"        //Authorization flag (there is a service definition that requires authorization) The business system requires that a specific authorization flag bit be set when authorization is performed, and the front-end authorization returns the authorization flag.
	TRGSERVERID       = "TrgServerId"       //Target system server ID (server name or IP address)
	TRGSERVICEID      = "TrgServiceId"      //Target service ID (Topoic or URL)
	TRGSERVICEVERSION = "TrgServiceVersion" //Target service version
	TRGTOPOICID       = "TrgTopoicId"       //Target Service ID (Topoic)
	TRGBIZDATE        = "TrgBizDate"        //Target system service business date
	TRGBIZSEQNO       = "TrgBizSeqNo"       //Target system business flow
	TRGTIMESTAMP      = "TrgTimeStamp"      //This field must be filled in when returning
	LASTACTENTRYNO    = "LastActEntryNo"    //Maintained by the account center, each system is transparent, and must be digital
	RETSTATUS         = "RetStatus"         //Value range: N-successful transaction F-failed transaction This field must be filled in when returning
	IS_NEED_LOOKUP    = "_is_need_lookup"
)

//Define common error message structures
const (
	RETMSGCODE  = "RetMsgCode"
	RETAUTHLVL  = "RetAuthLvl"
	RETMESSAGE  = "RetMessage"
	ERRORFORMID = "RETMSG"
)

//Define default language
const (
	ENUS = "en-US"
)

//Define common error codes
const (
	SYSPANIC = "SY00000001"
	SYSCODE1 = "SY00000002"

	SYTIMEOUT   = "SY00000003"
	SYERRCONT   = "SY00000004"
	KEYCONFLICT = "SY00000005"

	REQPACKERR   = "SY00000006"
	REQUNPACKERR = "SY00000007"
	RSPPACKERR   = "SY00000008"
	RSPUNPACKERR = "SY00000009"

	PROXYREGFAILD = "SY00000010" //Register DTS Proxy failed. %v
	PROXYFAILD    = "SY00000011" //DTS proxy executed service failed, %v
	REMOTEFAILD   = "SY00000012" //Remote call %s failed, error: %v
	INVALIDERR    = "SY00000013" //
	NOTFOUNDTOPIC = "SY00000014" //Can't found destination topic. serviceKey: [%v]

	DASNOTFOUND = "DA00000002"
)

const (
	TopicPrefix          = "topics::"
	RequestFormIdPrefix  = "requestForm::"
	ResponseFormIdPrefix = "responseForm"
)

//Define common header
const (
	DTS_TOPIC_TYPE = "DTS"
	TRN_TOPIC_TYPE = "TRN"
	KEY_TYPE_RSA   = "RSA"
	KEY_TYPE_AES   = "AES"
	DLS_TYPE_CMM   = "CMM"
	DLS_ID_COMMON  = "common"
	DLS_TYPE_LNM   = "LNM"				// 贷款切片类型
	DLS_LNM_COMMON = "ilcomm"			// 贷款公共ID
	DLS_TYPE_SCM   = "SCM"				//存款的公共
	DLS_TYPE_CON   = "CON"
	DLS_TYPE_ACC   = "ACC"
	DLS_TYPE_ACT   = "ACT"
	DLS_TYPE_CUS   = "CUS"
	DLS_TYPE_ACG   = "ACG"
	DLS_TYPE_PRD   = "PRD"
	DLS_TYPE_PHN   = "PHN"
	DLS_TYPE_IDN   = "IDN"
	DLS_TYPE_ODR   = "ODR" //FOR ORDER
	DLS_TYPE_MER   = "MER" //FOR merchant
	DLS_TYPE_PPS   = "PPS" //For PP service
	DLS_TYPE_HUI   = "HUI" //For HU id
	DLS_TYPE_ORG   = "ORG" //For organization
	DLS_TYPE_HCD   = "HCD" //For Hcode
	DLS_TYPE_WID   = "WID" //For wallet WalletId
	DLS_TYPE_CNT   = "CNT" // 合同号切片类型
	DLS_TYPE_DBT   = "DBT" // 借据号切片类型
	DLS_TYPE_AGM   = "AGM" //存款的-合约号切片类型
	DLS_TYPE_MED   = "MED"
)

//Request is the struct which describing the format from client
//the whole struct should be stored in SMF body
type Request struct {
	//Param is the param in HTTP request
	//especially useful in GET request
	Param map[string]string `json:"param"`
	//Header is the header will be put on HTTP request
	Header map[string]string `json:"header"`
	//Body is the body will be put on HTTP request
	Body []byte `json:"body"`
}

//Response is the struct which will be returned to client
//the whole struct will be stored in SMF body
type Response struct {
	//Status represent the HTTP status code, eg 200,401
	Status int `json:"status"`
	//Header represent HTTP header return from server
	Header map[string]interface{} `json:"header"`
	//Body represent HTTP body return from server
	Body []byte `json:"body"`
}

const HTTPSTATUSCODE = "httpStatusCode"
const (
	TIME_STAMP        = "2006-01-02 15:04:05"
	DATE_DASH_FORMAT  = "2006-01-02"
	DATEFORMAT        = "20060102"
	TIME_COLON_FORMAT = "15:04:05"
)

const (
	ContentType = "Content-Type"
	JsonContent = "application/json; charset=UTF-8"
)

const PaotangSuccess = "0000"

const BuddhistYearDiffer = 543

// ISO4217List is the list of ISO currency codes
var ISO4217List = map[string]struct{}{
	"AED": {}, "AFN": {}, "ALL": {}, "AMD": {}, "ANG": {}, "AOA": {}, "ARS": {}, "AUD": {}, "AWG": {}, "AZN": {},
	"BAM": {}, "BBD": {}, "BDT": {}, "BGN": {}, "BHD": {}, "BIF": {}, "BMD": {}, "BND": {}, "BOB": {}, "BOV": {}, "BRL": {}, "BSD": {}, "BTN": {}, "BWP": {}, "BYN": {}, "BZD": {},
	"CAD": {}, "CDF": {}, "CHE": {}, "CHF": {}, "CHW": {}, "CLF": {}, "CLP": {}, "CNY": {}, "COP": {}, "COU": {}, "CRC": {}, "CUC": {}, "CUP": {}, "CVE": {}, "CZK": {},
	"DJF": {}, "DKK": {}, "DOP": {}, "DZD": {},
	"EGP": {}, "ERN": {}, "ETB": {}, "EUR": {},
	"FJD": {}, "FKP": {},
	"GBP": {}, "GEL": {}, "GHS": {}, "GIP": {}, "GMD": {}, "GNF": {}, "GTQ": {}, "GYD": {},
	"HKD": {}, "HNL": {}, "HRK": {}, "HTG": {}, "HUF": {},
	"IDR": {}, "ILS": {}, "INR": {}, "IQD": {}, "IRR": {}, "ISK": {},
	"JMD": {}, "JOD": {}, "JPY": {},
	"KES": {}, "KGS": {}, "KHR": {}, "KMF": {}, "KPW": {}, "KRW": {}, "KWD": {}, "KYD": {}, "KZT": {},
	"LAK": {}, "LBP": {}, "LKR": {}, "LRD": {}, "LSL": {}, "LYD": {},
	"MAD": {}, "MDL": {}, "MGA": {}, "MKD": {}, "MMK": {}, "MNT": {}, "MOP": {}, "MRO": {}, "MUR": {}, "MVR": {}, "MWK": {}, "MXN": {}, "MXV": {}, "MYR": {}, "MZN": {},
	"NAD": {}, "NGN": {}, "NIO": {}, "NOK": {}, "NPR": {}, "NZD": {},
	"OMR": {},
	"PAB": {}, "PEN": {}, "PGK": {}, "PHP": {}, "PKR": {}, "PLN": {}, "PYG": {},
	"QAR": {},
	"RON": {}, "RSD": {}, "RUB": {}, "RWF": {},
	"SAR": {}, "SBD": {}, "SCR": {}, "SDG": {}, "SEK": {}, "SGD": {}, "SHP": {}, "SLL": {}, "SOS": {}, "SRD": {}, "SSP": {}, "STD": {}, "SVC": {}, "SYP": {}, "SZL": {},
	"THB": {}, "TJS": {}, "TMT": {}, "TND": {}, "TOP": {}, "TRY": {}, "TTD": {}, "TWD": {}, "TZS": {},
	"UAH": {}, "UGX": {}, "USD": {}, "USN": {}, "UYI": {}, "UYU": {}, "UZS": {},
	"VEF": {}, "VND": {}, "VUV": {},
	"WST": {},
	"XAF": {}, "XAG": {}, "XAU": {}, "XBA": {}, "XBB": {}, "XBC": {}, "XBD": {}, "XCD": {}, "XDR": {}, "XOF": {}, "XPD": {}, "XPF": {}, "XPT": {}, "XSU": {}, "XTS": {}, "XUA": {}, "XXX": {},
	"YER": {},
	"ZAR": {}, "ZMW": {}, "ZWL": {},
}
