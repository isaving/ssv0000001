package main

import (
	"fmt"
	"strings"
)

var templete = `
const (
	%s_REQUEST_DATA  = ` + "`" + "`" + `
	%s_RESPONSE_DATA = ` + "`" + "`" + `
)

func Test%sRequestForm(t *testing.T) {
	%sRequestForm := &%sRequestForm{}
	request := %s_REQUEST_DATA

	%sIFormData, err := %sRequestForm.UnPackRequest([]byte(request))

	if err != nil {
		t.Errorf("Test%sRequestForm, %sRequestForm.UnPackRequest failed:%v", err)
	}

	t.Logf("Request=[%++v]", %sIFormData)

	if req, err := %sRequestForm.PackRequest(%sIFormData); nil != err {
		t.Errorf("Test%sRequestForm, %sRequestForm.PackRequest failed:%v", err)
	} else {
		t.Logf("res:%s", string(req))
	}

	invalidRequest2 := "{ \"Form\": []}"

	_, err = %sRequestForm.UnPackRequest([]byte(invalidRequest2))

	if err == nil {
		t.Errorf("expect error return!")
	}

	invalidRequest3 := ""

	_, err = %sRequestForm.UnPackRequest([]byte(invalidRequest3))

	if err == nil {
		t.Errorf("expect error return!")
	}
}

func Test%sResponseForm(t *testing.T) {
	%sResponseForm := &%sResponseForm{}
	response := %s_RESPONSE_DATA
	%sOFormData, err := %sResponseForm.UnPackResponse([]byte(response))

	if err != nil {
		t.Errorf("Test%sResponseForm, %sResponseForm.UnPackResponse failed:%v", err)
	}

	t.Logf("Response=[%++v]", %sOFormData)

	if res, err := %sResponseForm.PackResponse(%sOFormData); nil != err {
		t.Errorf("Test%sResponseForm, %sResponseForm.PackResponse failed:%v", err)
	} else {
		t.Logf("res:%s", string(res))
	}

	invalidRequest2 := "{ \"Form\": []}"

	_, err = %sResponseForm.UnPackResponse([]byte(invalidRequest2))

	if err == nil {
		t.Errorf("expect error return!")
	}

	invalidRequest3 := ""

	_, err = %sResponseForm.UnPackResponse([]byte(invalidRequest3))

	if err == nil {
		t.Errorf("expect error return!")
	}
}
`

var serviceList = []string{
	"wl0txrc0",
	"cm0otpa0",
	"cm0ndcb1",
	"cm0idplu",
	"ac1dpp01",
	"ac1dpp02",
	"ac1dpp03",
	"ac1dpp04",
	"cm0otpv0",
	"cm0ref01",
	"cm1facmp",
	"cm1gtcol",
	"cm1lmt01",
	"cm1ndcrr",
	"cm1ndgas",
	"cm1ndgid",
	"cm1ndrpr",
	"cm1otprq",
	"cm1otpvd",
	"cm1rftlv",
	"cm1vrnid",
	"cu1perc1",
	"cu1peru1",
	"daaccdp0",
	"daacrdp0",
	"daacudp0",
	"dacmcfw0",
	"dacmcimg",
	"dacmcref",
	"dacmcrpi",
	"dacmcvri",
	"dacmdref",
	"dacmqial",
	"dacmqrpi",
	"dacmqrpr",
	"dacmrimg",
	"dacmrls1",
	"dacmrmi1",
	"dacmrotp",
	"dacmrprm",
	"dacmrrpt",
	"dacmrrsa",
	"dacmrseq",
	"dacmrtcv",
	"dacmrvri",
	"dacmuial",
	"dacmuotp",
	"dacmurpi",
	"dacmurpt",
	"dacmuvr2",
	"dacmuvri",
	"dacuc000",
	"dacur000",
	"dacur002",
	"dacurcc0",
	"dacuu000",
	"dasvcci0",
	"dasvccr0",
	"dasvctd0",
	"dasvctf0",
	"dasvrci1",
	"dasvrcr0",
	"dasvrpd0",
	"dawlcaj0",
	"dawlcbi0",
	"dawlcci0",
	"dawlcpn0",
	"dawlctch",
	"dawlctd1",
	"dawlrbi0",
	"dawlrci0",
	"dawlrtr0",
	"dawluaj0",
	"dawluci0",
	"dawlvpn0",
	"sv1accr1",
	"sv1acdr1",
	"sv1balq1",
	"sv1eaco1",
	"sv1trsf1",
	"wl0bcii0",
	"wl0crpr0",
	"wl0gcls0",
	"wl0home0",
	"wl0iali0",
	"wl0logn0",
	"wl0nidv0",
	"wl0oeac0",
	"wl0opai0",
	"wl0prtq0",
	"wl0rfll0",
	"wl0rgst0",
	"wl0rprq0",
	"wl0rpsi0",
	"wl0tcvi0",
	"wl0tcvu0",
	"wl0tfr01",
	"wl1fcpb0",
	"wl1pin01",
	"wl1pin02",
}

//var sn1 = "wl1pin02"
//var sn2 = "Wl1pin02"
//var sn3 = "WL1PIN02"

func main() {
	for _, service := range serviceList {
		sn1 := service
		sn2 := strings.ToUpper(service[0:1]) + service[1:]
		sn3 := strings.ToUpper(service)
		fmt.Printf("--------------------service:%s_test---------------------\n", service)
		str := fmt.Sprintf(templete,
			sn3,
			sn3,
			sn2, sn1, sn2, sn3, sn1, sn1, sn2, sn2, "%v", "%++v", sn1, sn1, sn1, sn2, sn2, "%v", "%s", sn1, sn1,
			sn2, sn1, sn2, sn3, sn1, sn1, sn2, sn2, "%v", "%++v", sn1, sn1, sn1, sn2, sn2, "%v", "%s", sn1, sn1,
		)

		fmt.Println(str)
		fmt.Printf("--------------------service:%s-end---------------------\n", service)
	}
}
