package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACZAN0I struct {
	AcctType   string ` validate:"required,max=10"`
	Bescribe   string
	AccType    string
	Subject    string
	AccSubType string
	DrCrFlag   string
	SubFlag    string
	IsLedger   string
	IsAccount  string
}

type DAACZAN0O struct {
	AcctType   string
	Bescribe   string
	AccType    string
	Subject    string
	AccSubType string
	DrCrFlag   string
	SubFlag    string
	IsLedger   string
	IsAccount  string

}

type DAACZAN0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACZAN0I
}

type DAACZAN0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACZAN0O
}

type DAACZAN0RequestForm struct {
	Form []DAACZAN0IDataForm
}

type DAACZAN0ResponseForm struct {
	Form []DAACZAN0ODataForm
}

// @Desc Build request message
func (o *DAACZAN0RequestForm) PackRequest(DAACZAN0I DAACZAN0I) (responseBody []byte, err error) {

	requestForm := DAACZAN0RequestForm{
		Form: []DAACZAN0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACZAN0I",
				},
				FormData: DAACZAN0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACZAN0RequestForm) UnPackRequest(request []byte) (DAACZAN0I, error) {
	DAACZAN0I := DAACZAN0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACZAN0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACZAN0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACZAN0ResponseForm) PackResponse(DAACZAN0O DAACZAN0O) (responseBody []byte, err error) {
	responseForm := DAACZAN0ResponseForm{
		Form: []DAACZAN0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACZAN0O",
				},
				FormData: DAACZAN0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACZAN0ResponseForm) UnPackResponse(request []byte) (DAACZAN0O, error) {

	DAACZAN0O := DAACZAN0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACZAN0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACZAN0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACZAN0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
