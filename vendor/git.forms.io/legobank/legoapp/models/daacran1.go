package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRAN1I struct {
	AcctgAcctNo		string	`validate:"max=20,required"`
}

type DAACRAN1O struct {
	AcctCreateDate        string `json:"AcctCreateDate"`
	AcctStatus            string `json:"AcctStatus"`
	AcctgAcctNo           string `json:"AcctgAcctNo"`
	AcctingOrgID          string `json:"AcctingOrgId"`
	Balance               float64    `json:"Balance"`
	BalanceTemp1          float64    `json:"BalanceTemp1"`
	BalanceTemp2          float64    `json:"BalanceTemp2"`
	BalanceYesterday      float64    `json:"BalanceYesterday"`
	BalanceYesterdayTemp1 float64    `json:"BalanceYesterdayTemp1"`
	BalanceYesterdayTemp2 float64    `json:"BalanceYesterdayTemp2"`
	CavAmt                float64    `json:"CavAmt"`
	ContID                string `json:"ContId"`
	CurrRepPrinTerm       int    `json:"CurrRepPrinTerm"`
	Currency              string `json:"Currency"`
	CustID                string `json:"CustId"`
	DrawDate              string `json:"DrawDate"`
	DrawTotBalance        int    `json:"DrawTotBalance"`
	LastMaintBrno         string `json:"LastMaintBrno"`
	LastMaintDate         string `json:"LastMaintDate"`
	LastMaintTell         string `json:"LastMaintTell"`
	LastMaintTime         string `json:"LastMaintTime"`
	LastTranDate          string `json:"LastTranDate"`
	MaturityDate          string `json:"MaturityDate"`
	MgmtOrgID             string `json:"MgmtOrgId"`
	NextPrinDate          string `json:"NextPrinDate"`
	OrgCreate             string `json:"OrgCreate"`
	ProdCode              string `json:"ProdCode"`
	RepPrinTotTerm        int    `json:"RepPrinTotTerm"`
	RepayFreq             string `json:"RepayFreq"`
	RepayType             string `json:"RepayType"`
	Status                string `json:"Status"`
	TccState              int    `json:"TccState"`
	UnpaidBal             int    `json:"UnpaidBal"`
	ValidStatus           string `json:"ValidStatus"`
}

type DAACRAN1IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRAN1I
}

type DAACRAN1ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRAN1O
}

type DAACRAN1RequestForm struct {
	Form []DAACRAN1IDataForm
}

type DAACRAN1ResponseForm struct {
	Form []DAACRAN1ODataForm
}

// @Desc Build request message
func (o *DAACRAN1RequestForm) PackRequest(DAACRAN1I DAACRAN1I) (responseBody []byte, err error) {

	requestForm := DAACRAN1RequestForm{
		Form: []DAACRAN1IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRAN1I",
				},
				FormData: DAACRAN1I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRAN1RequestForm) UnPackRequest(request []byte) (DAACRAN1I, error) {
	DAACRAN1I := DAACRAN1I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRAN1I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRAN1I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRAN1ResponseForm) PackResponse(DAACRAN1O DAACRAN1O) (responseBody []byte, err error) {
	responseForm := DAACRAN1ResponseForm{
		Form: []DAACRAN1ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRAN1O",
				},
				FormData: DAACRAN1O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRAN1ResponseForm) UnPackResponse(request []byte) (DAACRAN1O, error) {

	DAACRAN1O := DAACRAN1O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRAN1O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRAN1O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRAN1I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
