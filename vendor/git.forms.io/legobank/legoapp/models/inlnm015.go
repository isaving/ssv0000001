package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type INLNM015I struct {
	Op                  string `valid:"MaxSize(1)"` // 操作标识（枚举值）  1-新增、2-修改，3-失效
	AcctgAcctNo         string `valid:"MaxSize(20)"`  //贷款核算账号
	IntStatus			string `valid:"MaxSize(3)"`  //利息状态
	PrinStatus          string `valid:"MaxSize(1)"`   //本金状态
	IntPlanNo           string `valid:"MaxSize(20)"`  //计息规则编号(新增是没有这个字段)
	BankNo              string `valid:"MaxSize(20)"`  //机构号
	Currency            string `valid:"MaxSize(3)"`   // 币种代码
	IntFlag             string `valid:"MaxSize(1)"`   //计息优先类型代码
	IntCalcOption       string `valid:"MaxSize(2)"`   //计息算法类型代码
	MinIntAmt           float64                       //`valid:"Required"`              //最低结息金额 19
	LyrdFlag            string `valid:"MaxSize(1)"`   //分层标志
	AmtLvlsFlag         string `valid:"MaxSize(1)"`   //金额层级类型代码
	TermLvlsFlag        string `valid:"MaxSize(1)"`   //期限层级类型代码
	TermAmtPrtyFlag     string `valid:"MaxSize(1)"`   //期限金额优先类型代码
	IntRateUseFlag      string `valid:"MaxSize(1)"`   //使用利率来源代码
	IntRateNo           string `valid:"MaxSize(20)"`  //利率编号
	FixdIntRate         float64                       //`valid:"Required"`              //固定利率 9 6
	MinIntRate          float64                       //`valid:"Required"`              //最低利率 9 6
	MonIntUnit          string `valid:"MaxSize(1)"`   //月计息天数代码
	YrIntUnit           string `valid:"MaxSize(1)"`   //年计息天数代码
	FloatOption         string `valid:"MaxSize(1)"`   //浮动种类代码
	FloatFlag           string `valid:"MaxSize(2)"`   //浮动方向代码
	FloatCeil           float64                       //`valid:"Required"`              //浮动上限值 19
	FloatFloor          float64                           //`valid:"Required"`              //浮动下限值
	DefFloatRate        float64                       //`valid:"Required"`              //默认浮动值 10
	CornerFlag          string `valid:"MaxSize(1)"`   //分角位参与计息标志
	CritocalPointFlag   string `valid:"MaxSize(1)"`   //临界点区间分类代码
	LvlsIntRateAmtType  string `valid:"MaxSize(2)"`   //层级利率金额类型代码
	TolLvls             int64                           //`valid:"Required"`              //层级总数量
	LyrdTermUnit        string `valid:"MaxSize(2)"`   //分层期限单位代码
	LoanIntRateAdjType  string `valid:"MaxSize(2)"`   //使用利率类型代码
	LoanIntRateAdjCycle string `valid:"MaxSize(1)"`   //贷款利率调整周期代码
	LoanIntRateAdjFreq  string `valid:"MaxSize(1)"`   //贷款利率调整周期数量
	Flag1				string `valid:"MaxSize(1)"`   //标志1
	Flag2				string `valid:"MaxSize(1)"`   //标志1
	Back1				float64    //备用1
	Back2				float64    //备用2
	Back3				string `valid:"MaxSize(1)"`   //备用3
	LastMaintDate		string `valid:"MaxSize(10)"`   //最后更新日期
	LastMaintTime		string `valid:"MaxSize(19)"`   //最后更新时间
	LastMaintBrno		string `valid:"MaxSize(20)"`   //最后更新机构
	LastMaintTell		string `valid:"MaxSize(30)"`   //最后更新柜员
}

type INLNM015O struct {
	TxDt	string
	TxSn	string
}

type INLNM015IDataForm struct {
	FormHead CommonFormHead
	FormData INLNM015I
}

type INLNM015ODataForm struct {
	FormHead CommonFormHead
	FormData INLNM015O
}

type INLNM015RequestForm struct {
	Form []INLNM015IDataForm
}

type INLNM015ResponseForm struct {
	Form []INLNM015ODataForm
}

// @Desc Build request message
func (o *INLNM015RequestForm) PackRequest(INLNM015I INLNM015I) (responseBody []byte, err error) {

	requestForm := INLNM015RequestForm{
		Form: []INLNM015IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "INLNM015I",
				},
				FormData: INLNM015I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *INLNM015RequestForm) UnPackRequest(request []byte) (INLNM015I, error) {
	INLNM015I := INLNM015I{}
	if err := json.Unmarshal(request, o); nil != err {
		return INLNM015I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return INLNM015I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *INLNM015ResponseForm) PackResponse(INLNM015O INLNM015O) (responseBody []byte, err error) {
	responseForm := INLNM015ResponseForm{
		Form: []INLNM015ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "INLNM015O",
				},
				FormData: INLNM015O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *INLNM015ResponseForm) UnPackResponse(request []byte) (INLNM015O, error) {

	INLNM015O := INLNM015O{}

	if err := json.Unmarshal(request, o); nil != err {
		return INLNM015O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return INLNM015O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *INLNM015I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
