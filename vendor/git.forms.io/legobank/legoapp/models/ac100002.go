package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC100002I struct {
	AcctgAcctNo		string	`validate:"required"`//核算账号
	RepayType		string	//还款周期
	RepayFreq		string	//还款频率
	RepPrinTotTerm	string	//还本总期数
	NextPrinDate	string	//下一还本日
	MaturityDate	string	//到期日
}

type AC100002O struct {
	Status	string
}

type AC100002IDataForm struct {
	FormHead CommonFormHead
	FormData AC100002I
}

type AC100002ODataForm struct {
	FormHead CommonFormHead
	FormData AC100002O
}

type AC100002RequestForm struct {
	Form []AC100002IDataForm
}

type AC100002ResponseForm struct {
	Form []AC100002ODataForm
}

// @Desc Build request message
func (o *AC100002RequestForm) PackRequest(AC100002I AC100002I) (responseBody []byte, err error) {

	requestForm := AC100002RequestForm{
		Form: []AC100002IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC100002I",
				},
				FormData: AC100002I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC100002RequestForm) UnPackRequest(request []byte) (AC100002I, error) {
	AC100002I := AC100002I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC100002I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC100002I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC100002ResponseForm) PackResponse(AC100002O AC100002O) (responseBody []byte, err error) {
	responseForm := AC100002ResponseForm{
		Form: []AC100002ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC100002O",
				},
				FormData: AC100002O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC100002ResponseForm) UnPackResponse(request []byte) (AC100002O, error) {

	AC100002O := AC100002O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC100002O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC100002O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC100002I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
