package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACVAN0I struct {
	TransactionCode string ` validate:"required,max=10"`
	Describe        string
	AcctType        string
	AcctIntl        string
	IsRealTime      string
}

type DAACVAN0O struct {
	TransactionCode string
	Describe        string
	AcctType        string
	AcctIntl        string
	IsRealTime      string

}

type DAACVAN0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACVAN0I
}

type DAACVAN0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACVAN0O
}

type DAACVAN0RequestForm struct {
	Form []DAACVAN0IDataForm
}

type DAACVAN0ResponseForm struct {
	Form []DAACVAN0ODataForm
}

// @Desc Build request message
func (o *DAACVAN0RequestForm) PackRequest(DAACVAN0I DAACVAN0I) (responseBody []byte, err error) {

	requestForm := DAACVAN0RequestForm{
		Form: []DAACVAN0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACVAN0I",
				},
				FormData: DAACVAN0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACVAN0RequestForm) UnPackRequest(request []byte) (DAACVAN0I, error) {
	DAACVAN0I := DAACVAN0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACVAN0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACVAN0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACVAN0ResponseForm) PackResponse(DAACVAN0O DAACVAN0O) (responseBody []byte, err error) {
	responseForm := DAACVAN0ResponseForm{
		Form: []DAACVAN0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACVAN0O",
				},
				FormData: DAACVAN0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACVAN0ResponseForm) UnPackResponse(request []byte) (DAACVAN0O, error) {

	DAACVAN0O := DAACVAN0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACVAN0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACVAN0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACVAN0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
