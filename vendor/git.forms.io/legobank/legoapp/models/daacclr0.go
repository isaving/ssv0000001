package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCLR0I struct {
	AcctgAcctNo   string `validate:"required,max=32"`
	Status        string `validate:"required,max=32"`
	Currency      string
	WorkDate      string
	MgmtOrgId     string
	AcctingOrgId  string
	BizFolnNo     string
	SysFolnNo     string
	ChnlType      string
	IntRepayAmt   float64
	PeriodNum     int64
	BalanceType   string
	IntRepayType  string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string
	RecordNo      int64
}

type DAACCLR0O struct {
	AcctgAcctNo   string
	Status        string
	Currency      string
	WorkDate      string
	MgmtOrgId     string
	AcctingOrgId  string
	BizFolnNo     string
	SysFolnNo     string
	ChnlType      string
	IntRepayAmt   float64
	PeriodNum     int64
	BalanceType   string
	IntRepayType  string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string
	RecordNo      int64
}

type DAACCLR0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCLR0I
}

type DAACCLR0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCLR0O
}

type DAACCLR0RequestForm struct {
	Form []DAACCLR0IDataForm
}

type DAACCLR0ResponseForm struct {
	Form []DAACCLR0ODataForm
}

// @Desc Build request message
func (o *DAACCLR0RequestForm) PackRequest(DAACCLR0I DAACCLR0I) (responseBody []byte, err error) {

	requestForm := DAACCLR0RequestForm{
		Form: []DAACCLR0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCLR0I",
				},
				FormData: DAACCLR0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCLR0RequestForm) UnPackRequest(request []byte) (DAACCLR0I, error) {
	DAACCLR0I := DAACCLR0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCLR0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCLR0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCLR0ResponseForm) PackResponse(DAACCLR0O DAACCLR0O) (responseBody []byte, err error) {
	responseForm := DAACCLR0ResponseForm{
		Form: []DAACCLR0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCLR0O",
				},
				FormData: DAACCLR0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCLR0ResponseForm) UnPackResponse(request []byte) (DAACCLR0O, error) {

	DAACCLR0O := DAACCLR0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCLR0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCLR0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCLR0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
