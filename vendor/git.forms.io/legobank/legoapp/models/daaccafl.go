package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCAFLI struct {
	HostTranSerialNo          string `validate:"required,max=42"`
	BussDate                  string
	BussTime                  string
	HostTranSeq               int64
	PeripheralSysWorkday      string
	PeripheralSysWorktime     string
	PeripheralTranSerialNo    string
	PeripheralTranSeq         float64
	BookedWorkday             string
	AcctingTime               string
	LiquidationDate           string
	LiquidationTime           string
	TranOrgId                 string
	AgentOrgId                string
	TranTeller                string
	AuthTeller                string
	ReviewTeller              string
	TranChannel               string
	AccessChannel             string
	TerminalNo                string
	BussSys                   string
	TrasactionCode            string
	FunctionCode              string
	ApprovalNo                string
	ClearingBussType          string
	ProdCode                  string
	ProdSeq                   float64
	BussType                  string
	BussCategories            string
	DebitFlag                 string
	LocalBankFlag             string
	Country                   string
	OtherBankFlag             string
	AcctingOrgId              string
	LocalAcctDiff             string
	LocalTranDetailType       string
	CustId                    string
	TheMerchantNo             string
	BussAcctNo                string
	AcctNo                    string
	AcctgAcctNo               string
	SubjectNo                 string
	SubjectBreakdown          string
	MediaType                 string
	LocalMediaPrefix          string
	MediaNo                   string
	CustDiff                  string
	AmtType                   string
	DurationOfDep             float64
	DaysOfDep                 float64
	AcctingCode1              string
	AcctingCode2              string
	AcctingCode3              string
	AcctingCode4              string
	EventIndication           string
	EventBreakdown            string
	CustEvent1                string
	CustEvent2                string
	Currency                  string
	BanknoteFlag              string
	Amt                       float64
	AcctBal                   float64
	CashTranFlag              string
	ValueDate                 string
	ExchRestType              string
	AntiTradingMark           string
	CorrectTradingFlag        string
	OriginalTradingDay        string
	OriginalHostTranSerialNo  float64
	OriginalTradeTellerNo     string
	AcctManagerNo             string
	MessageCode               string
	AbstractCode              string
	UseCode                   string
	SetNo                     string
	WhetherToAllowPunching    string
	ChannelCorrectControlFlag string
	IsSendClearingFlag        string
	IsMakeUpForPayables       string
	LastMaintDate             string
	LastMaintTime             string
	LastMaintBrno             string
	LastMaintTell             string
	BalanceType               string
	RepayPrincipal            float64
	RepayInterest             float64
	LastTranDate              string
}

type DAACCAFLO struct {
	Status	string
}

type DAACCAFLIDataForm struct {
	FormHead CommonFormHead
	FormData DAACCAFLI
}

type DAACCAFLODataForm struct {
	FormHead CommonFormHead
	FormData DAACCAFLO
}

type DAACCAFLRequestForm struct {
	Form []DAACCAFLIDataForm
}

type DAACCAFLResponseForm struct {
	Form []DAACCAFLODataForm
}

// @Desc Build request message
func (o *DAACCAFLRequestForm) PackRequest(DAACCAFLI DAACCAFLI) (responseBody []byte, err error) {

	requestForm := DAACCAFLRequestForm{
		Form: []DAACCAFLIDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCAFLI",
				},
				FormData: DAACCAFLI,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCAFLRequestForm) UnPackRequest(request []byte) (DAACCAFLI, error) {
	DAACCAFLI := DAACCAFLI{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCAFLI, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCAFLI, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCAFLResponseForm) PackResponse(DAACCAFLO DAACCAFLO) (responseBody []byte, err error) {
	responseForm := DAACCAFLResponseForm{
		Form: []DAACCAFLODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCAFLO",
				},
				FormData: DAACCAFLO,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCAFLResponseForm) UnPackResponse(request []byte) (DAACCAFLO, error) {

	DAACCAFLO := DAACCAFLO{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCAFLO, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCAFLO, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCAFLI) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
