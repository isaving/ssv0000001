package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACMAN0I struct {
	AcctgID         string	`validate:"required,max=40"`
	AcctgSeqNo      int		`validate:"required"`
	AcctgSubNo      int		`validate:"required"`
}

type DAACMAN0O struct {
	Account         string `json:"Account"`
	AcctCode        string `json:"AcctCode"`
	AcctDate        string `json:"AcctDate"`
	AcctSubject     string `json:"AcctSubject"`
	AcctType        string `json:"AcctType"`
	AcctgID         string `json:"AcctgID"`
	AcctgSeqNo      int    `json:"AcctgSeqNo"`
	AcctgSubNo      int    `json:"AcctgSubNo"`
	AcctingDate     string `json:"AcctingDate"`
	AcctingDateTime string `json:"AcctingDateTime"`
	Amount          int    `json:"Amount"`
	AssInputter     string `json:"AssInputter"`
	AssorInputter   string `json:"AssorInputter"`
	BussDate        string `json:"BussDate"`
	CoCode          string `json:"CoCode"`
	CoCompany       string `json:"CoCompany"`
	CorAcctDate     string `json:"CorAcctDate"`
	CorAcctNo       string `json:"CorAcctNo"`
	CrAcctFlag      string `json:"CrAcctFlag"`
	Currency        string `json:"Currency"`
	DrCrFlag        string `json:"DrCrFlag"`
	EntryType       string `json:"EntryType"`
	Inputter        string `json:"Inputter"`
	InterDate       string `json:"InterDate"`
	IsAcctCenter    string `json:"IsAcctCenter"`
	IsAcctSucc      string `json:"IsAcctSucc"`
	IsOutFlag       string `json:"IsOutFlag"`
	LastMaintBrno   string `json:"LastMaintBrno"`
	LastMaintDate   string `json:"LastMaintDate"`
	LastMaintTell   string `json:"LastMaintTell"`
	LastMaintTime   string `json:"LastMaintTime"`
	OrgScenarioID   string `json:"OrgScenarioId"`
	RAcctDate       string `json:"RAcctDate"`
	RAcctFlag       string `json:"RAcctFlag"`
	RAcctNo         string `json:"RAcctNo"`
	RAcctingNo      string `json:"RAcctingNo"`
	RControFlag     string `json:"RControFlag"`
	Remark          string `json:"Remark"`
	Spare1          string `json:"Spare1"`
	Spare2          string `json:"Spare2"`
	Spare3          string `json:"Spare3"`
	SrcBizSeqNo     string `json:"SrcBizSeqNo"`
	Subject         string `json:"Subject"`
	SumID           string `json:"SumId"`
	Summary         string `json:"Summary"`
	SummaryNo       string `json:"SummaryNo"`
	SysID           string `json:"SysId"`
	TccState        int    `json:"TccState"`
}

type DAACMAN0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACMAN0I
}

type DAACMAN0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACMAN0O
}

type DAACMAN0RequestForm struct {
	Form []DAACMAN0IDataForm
}

type DAACMAN0ResponseForm struct {
	Form []DAACMAN0ODataForm
}

// @Desc Build request message
func (o *DAACMAN0RequestForm) PackRequest(DAACMAN0I DAACMAN0I) (responseBody []byte, err error) {

	requestForm := DAACMAN0RequestForm{
		Form: []DAACMAN0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACMAN0I",
				},
				FormData: DAACMAN0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACMAN0RequestForm) UnPackRequest(request []byte) (DAACMAN0I, error) {
	DAACMAN0I := DAACMAN0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACMAN0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACMAN0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACMAN0ResponseForm) PackResponse(DAACMAN0O DAACMAN0O) (responseBody []byte, err error) {
	responseForm := DAACMAN0ResponseForm{
		Form: []DAACMAN0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACMAN0O",
				},
				FormData: DAACMAN0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACMAN0ResponseForm) UnPackResponse(request []byte) (DAACMAN0O, error) {

	DAACMAN0O := DAACMAN0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACMAN0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACMAN0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACMAN0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
