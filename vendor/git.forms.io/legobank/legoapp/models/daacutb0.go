package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACUTB0I struct {
	//输入是个map
}

type DAACUTB0O struct {

}

type DAACUTB0IDataForm struct {
	FormHead CommonFormHead
	FormData map[string]interface{}
}

type DAACUTB0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACUTB0O
}

type DAACUTB0RequestForm struct {
	Form []DAACUTB0IDataForm
}

type DAACUTB0ResponseForm struct {
	Form []DAACUTB0ODataForm
}

// @Desc Build request message
func (o *DAACUTB0RequestForm) PackRequest(DAACUTB0I map[string]interface{}) (responseBody []byte, err error) {

	requestForm := DAACUTB0RequestForm{
		Form: []DAACUTB0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUTB0I",
				},
				FormData: DAACUTB0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACUTB0RequestForm) UnPackRequest(request []byte) (map[string]interface{}, error) {
	DAACUTB0I := make(map[string]interface{})
	if err := json.Unmarshal(request, o); nil != err {
		return DAACUTB0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUTB0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACUTB0ResponseForm) PackResponse(DAACUTB0O DAACUTB0O) (responseBody []byte, err error) {
	responseForm := DAACUTB0ResponseForm{
		Form: []DAACUTB0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUTB0O",
				},
				FormData: DAACUTB0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACUTB0ResponseForm) UnPackResponse(request []byte) (DAACUTB0O, error) {

	DAACUTB0O := DAACUTB0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACUTB0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUTB0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACUTB0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
