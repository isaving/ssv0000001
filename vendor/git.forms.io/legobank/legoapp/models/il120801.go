package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type IL120801IDataForm struct {
	FormHead CommonFormHead
	FormData IL120801I
}

type IL120801ODataForm struct {
	FormHead CommonFormHead
	FormData IL120801O
}

type IL120801RequestForm struct {
	Form []IL120801IDataForm
}

type IL120801ResponseForm struct {
	Form []IL120801ODataForm
}
type IL120801I struct {
	ChanEleType    		string`validate:"required"`
	CustNo      		string`validate:"required"`
	LoanDubilNo 		string`validate:"required"`
}

type IL120801O struct{
	FileKey   string
}

// @Desc Build request message
func (o *IL120801RequestForm) PackRequest(iL120801I IL120801I) (responseBody []byte, err error) {

	requestForm := IL120801RequestForm{
		Form: []IL120801IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL120801I",
				},
				FormData: iL120801I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *IL120801RequestForm) UnPackRequest(request []byte) (IL120801I, error) {
	iIL120801I := IL120801I{}
	if err := json.Unmarshal(request, o); nil != err {
		return iIL120801I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return iIL120801I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *IL120801ResponseForm) PackResponse(iL120801O IL120801O) (responseBody []byte, err error) {
	responseForm := IL120801ResponseForm{
		Form: []IL120801ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL120801O",
				},
				FormData: iL120801O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *IL120801ResponseForm) UnPackResponse(request []byte) (IL120801O, error) {

	iL120801O := IL120801O{}

	if err := json.Unmarshal(request, o); nil != err {
		return iL120801O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return iL120801O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *IL120801I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
