package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type IL111007IDataForm struct {
	FormHead CommonFormHead
	FormData IL111007I
}

type IL111007ODataForm struct {
	FormHead CommonFormHead
	FormData IL111007O
}

type IL111007RequestForm struct {
	Form []IL111007IDataForm
}

type IL111007ResponseForm struct {
	Form []IL111007ODataForm
}

type IL111007I struct {
	LoanDubilNo  string  `validate:"required",json:"LoanDubilNo"`  //借据号
	Pridnum      int     `validate:"required",json:"Pridnum"`      //期次号
	ActlstPrin   float64 `json:"ActlstPrin"`                       //实际还款本金
	ActlstIntr   float64 `json:"ActlstIntr"`                       //实际还款计息
	KeprcdStusCd string  `validate:"required",json:"KeprcdStusCd"` //还款状态
	PayOffDt	 string  `validate:"required",json:"PayOffDt"` 	   //结清日期
}

type IL111007O struct {
	State string `json:"State"` // 返回状态
}

// @Desc Build request message
func (o *IL111007RequestForm) PackRequest(il111007I IL111007I) (responseBody []byte, err error) {

	requestForm := IL111007RequestForm{
		Form: []IL111007IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL111007I",
				},
				FormData: il111007I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *IL111007RequestForm) UnPackRequest(request []byte) (IL111007I, error) {
	il111007I := IL111007I{}
	if err := json.Unmarshal(request, o); nil != err {
		return il111007I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return il111007I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *IL111007ResponseForm) PackResponse(il111007O IL111007O) (responseBody []byte, err error) {
	responseForm := IL111007ResponseForm{
		Form: []IL111007ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL111007O",
				},
				FormData: il111007O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *IL111007ResponseForm) UnPackResponse(request []byte) (IL111007O, error) {

	il111007O := IL111007O{}

	if err := json.Unmarshal(request, o); nil != err {
		return il111007O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return il111007O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *IL111007I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
