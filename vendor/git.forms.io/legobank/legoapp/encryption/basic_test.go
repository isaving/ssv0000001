package encryption

import (
	"testing"
)

var otpRequestPubKey = `
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzyLGZS8Isnmesk+OE0i2
QyaYliYtTwOxTQRBZDug5EGJDt1A46IKsAOsJE9Bkhx4PgbZrl6MGYUiJi92oqWs
U9ov3ZBpRbc6T4V8Chi6Xka9b8c0OeDVFuB5Rct3AdW2bcITpcm9Cjd4KrGE9ISN
6ZxDCnsi/zdlGPdIPPAuuoNlSYEV4JnJdfY71d3Zh9c22HLcPaskCC6rxm3f0Sev
2lt3cYPUNeY7sS4QcdKnjlHKQSZsEoHRW0FfGhus94Q8WT+2WnfhMXMxjaeizNtL
U01UndzwlN4VgIc0pdqoLZRG5icMSyJyoIV0ZgJdWvuln0xDLfKfAOjmoJeZAcQ/
zQIDAQAB
-----END PUBLIC KEY-----`

var otpRawResponse = `{
    "headerResp": {
        "reqID": "ugKWCcYOZ0METCjH1hAsn",
        "reqDtm": "2019-10-15 15: 10: 28.156",
        "txnRefID": null,
        "service": "RequestOTPService",
        "statusCd": "0000",
        "statusDesc": "SUCCESS"
    },
    "otpResp": {
        "refNo": "C4EF",
        "expiryDate": "03/12/2019 20: 54: 22"
    }
}`

type OTPRequest struct {
	HeaderReq HeaderReq `json:"headerReq"` //header for request
	OtpReq    OtpReq    `json:"otpReq"`    //body response validate object
}

type OtpReq struct {
	Lang         string `json:"lang"`         //language of OTP
	MobileNumber string `json:"mobileNumber"` //mobile phone number
	OtpType      string `json:"otpType"`      //use AUT
	AppID        string `json:"appId"`        //generate random 56 characters
	UserID       string `json:"userId"`
	UUID         string `json:"uuId"` //Generated UUID
}

type HeaderReq struct {
	CardNo     string `json:"cardNo"`
	Ip         string `json:"ip"`         //The source of the device requested
	ReqBy      string `json:"reqBy"`      //mobile phone number
	ReqChannel string `json:"reqChannel"` //Channel name
	ReqDtm     string `json:"reqDtm"`     //Current Datetime (GMT +7) (Local TH)
	ReqID      string `json:"reqID"`      //Unique ID
	Service    string `json:"service"`    //Specify RequestOTP Service
	SofType    string `json:"sofType"`
}

type HeaderResp struct {
	ReqID      string `json:"reqID"`
	ReqDtm     string `json:"reqDtm"`
	TxnRefID   string `json:"txnRefID"`
	Service    string `json:"service"`
	StatusCd   string `json:"statusCd"`
	StatusDesc string `json:"statusDesc"`
}

type OTPResponse struct {
	HeaderResp HeaderResp `json:"headerResp"`
	OtpResp    OtpResp    `json:"otpResp"`
}

type OtpResp struct {
	RefNo      string `json:"refNo"`
	ExpiryDate string `json:"expiryDate"`
}

func TestPaotangAPIHandle_EncryptDecryptMessage(t *testing.T) {

	GlobalPublicKey = []byte(otpRequestPubKey)
	paotangAPIHandler := NewPaotangAPIHandler()
	otpRequest := OTPRequest{
		HeaderReq: HeaderReq{
			CardNo:     "",
			Ip:         "",
			ReqBy:      "0954024887",
			ReqChannel: "PTANDROID",
			ReqDtm:     "2019-10-15 15:10:28.156",
			ReqID:      "ugKWCcYOZ0METCjH1hAsn",
			Service:    "RequestOTPService",
			SofType:    "",
		},
		OtpReq:    OtpReq{
			Lang:         "th",
			MobileNumber: "0954024887",
			OtpType:      "AUT",
			AppID:        "dFlB97hv74IPPGEKqwFDUhxIowPhVZz6ZT4CFxWNg2Vzu6cQMMLOdDjjP",
			UserID:       "",
			UUID:         "eb091648-b046-4c17-a899-f2a0c29505f9",
		},
	}
	encryptMsg, err := paotangAPIHandler.EncryptMessage(otpRequest)
	if err != nil {
		t.Error(err)
	} else {
		t.Logf("encryptMsg: %s", encryptMsg)
	}
	aesIns := paotangAPIHandler.getAESIns()
	encryptRespMsg, err := aesIns.Encrypt([]byte(otpRawResponse))
	if err != nil {
		t.Error(err)
	}
	otpResponse := &OTPResponse{}

	err = paotangAPIHandler.DecryptMessage(encryptRespMsg, otpResponse)
	if err != nil {
		t.Error(err)
	} else {
		t.Logf("otpResponse: %++v", otpResponse)
	}

}