module git.forms.io/legobank/legoapp

go 1.13

require (
	git.forms.io/universe v0.0.0
	github.com/Unknwon/goconfig v0.0.0-20191126170842-860a72fb44fd // indirect
	github.com/astaxie/beego v1.12.2
	github.com/beego/i18n v0.0.0-20161101132742-e9308947f407
	github.com/json-iterator/go v1.1.10
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.6.0
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/valyala/fasthttp v1.14.0
)

replace git.forms.io/universe v0.0.0 => ../../../git.forms.io/universe
