package config

import (
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/sirupsen/logrus"
	"testing"
)

func TestInitLocaleLang(t *testing.T) {

	logrus.SetLevel(logrus.DebugLevel)
	log.InitLog()

	if err := InitLocaleLang(); err != nil {
		t.Error(err)
	}

}
