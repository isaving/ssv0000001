package main

import (
	"git.forms.io/universe/common/event_handler"
	"git.forms.io/universe/common/event_handler/base"
	"git.forms.io/universe/common/json"
    "git.forms.io/universe/dts/client/imports"
	"git.forms.io/legobank/legoapp/config"
	"git.forms.io/universe/solapp-sdk/heartbeat"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego"
	"github.com/urfave/cli"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"git.forms.io/isaving/sv/ssv0000001/routers"
)

var (
	APP_NAME = "ssv0000001"
	VERSION  = "v0.0.1"
)

func Init() error {
	if err := log.InitLog(); err != nil {
		log.Errorf("InitLog failed err=%v", err)
		return err
	}

	if err := config.InitServiceConfig(); err != nil {
		log.Errorf("Init solapp config failed, err=%v", err)
		return err
	}
    if err := imports.EnableDTSSupports(); err != nil {
		log.Errorf("Enable DTS supports failed err=%v", err)
		return err
	}
	if err := routers.InitRouter(); err != nil {
		log.Errorf("init services routers failed error message [%v]", err)
		return err
	}

	if err := event_handler.InitEventReceiver(); err != nil {
		log.Errorf("init event receiver failed err: %v.", err)
		return err
	}

	heartbeat.StartHeartbeat()
	return nil
}

func apiNotFound(rw http.ResponseWriter, r *http.Request) {
	output := base.BuildErrorResponse("API not found!")
	res, _ := json.Marshal(output)
	rw.Write(res)
}

func runBeego(c *cli.Context) error {
	if err := Init(); err != nil {
		log.Errorf("main init failed err=%v", err)
		panic(err)
	}
	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}
	beego.ErrorHandler("404", apiNotFound)
	beego.Run()
	return nil
}

func newApp(appName string, version string) *cli.App {
	app := cli.NewApp()
	app.Version = version
	app.Usage = appName
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "conf",
			Usage:  "Basic application configuration",
			EnvVar: "APP_CONF",
			Value:  "conf/app.conf",
		},
	}

	app.Action = runBeego
	app.Commands = []cli.Command{}

	return app
}

func main() {
	app := newApp(APP_NAME, VERSION)
	go func() {
		if err := app.Run(os.Args); err != nil {
			log.Errorf("Run App error %s", err.Error())
		}
	}()

	signals := make(chan (os.Signal), 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)
	signal := <-signals
	log.Infof("Received signal %s and exist", signal.String())
	return
}

