////Version: v0.0.1
package routers

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/isaving/sv/ssv0000001/controllers"
	"git.forms.io/universe/common/event_handler/register"
	"github.com/astaxie/beego"
)

// @Desc Select the appropriate transaction route according to TOPIC
// @Author
// @Date 2020-12-04
func InitRouter() error {

	eventRouteReg := register.NewEventHandlerRegister()

	bc := beego.AppConfig

	eventRouteReg.Router(bc.DefaultString(constant.TopicPrefix + "ssv0000001","SV000001"),
		&controllers.Ssv0000001Controller{}, "Ssv0000001")

	return nil
}

// @Desc transaction router for swagger
// @Author
// @Date 2020-12-04
func init() {

	ns := beego.NewNamespace("isaving/v1/xxxxxx",
		beego.NSNamespace("/xxxxxx",
			beego.NSInclude(
				&controllers.Ssv0000001Controller{},
			),
		),
	)
	beego.AddNamespace(ns)
}
