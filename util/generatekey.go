package util

import (
	"crypto/sha256"
	"golang.org/x/crypto/pbkdf2"
)

func GenerateKey(pinData string) []byte{

	salt := []byte("1234567890ABCDEF1234567890ABCDEF")
	dk := pbkdf2.Key([]byte(pinData), salt, 1024, 32, sha256.New)
	return dk
}