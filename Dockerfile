FROM alpine
COPY bin /data/app/
WORKDIR /data/app
CMD ["./ssv0000001"]

RUN addgroup -g 1000 -S universe \
    && adduser  universe -u 1000 -D -S -s /bin/bash -G universe \
    && chown universe:universe /data -R

USER 1000
